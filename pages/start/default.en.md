---
title: 'Welcome to the FairCoop Wiki'
---

<jumbotron>We are building WIKI pages for FAQs and more</jumbotron>

 
Until the pages are being filled, have a look at 

* [[https://2017.fair.coop|Website until 2017]]
* https://git.fairkom.net/faircoop (many projects have WIKI pages)
