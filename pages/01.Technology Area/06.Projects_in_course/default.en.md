---
title: 'Project in course'
0: 'tittle:''Projects in progress'''
---

We are an open community and we would love for you to join our projects.
We have now at least four different sites to follow our projects, I try to organize them a bit for better understanding:
  - Gitlab on FAIRKOM-> https://git.fairkom.net/faircoop/Tech
  - Github.com-> https://github.com/FairCoinTeam
  - OCP-> https://ocp.freedomcoop.eu/work/agent/475/
  - Wekan-> http://techs.sandcats.io:6080/shared/ZuZZ2DCO1D5CzTQtlzqqVeaUP7k54Ua6lOo5jDGwzGU/b/sandstorm/libreboard

## Fairmarket
Fairmarket team - doing support in 4 languages, maintenance, developing new features, and administering the website.

  * You can donate for this project: fGW3XeqWUzD1yvP3qpRntABpiLtbsam41Y
  * Website on production: https://market.fair.coop
  * Develop version:
  * Sources: (some modules and odoo version 8)
    * https://github.com/Punto0/
    * https://github.com/OCA/OCB/tree/8.0
  * Programming languages: python and javascript
  * Contact:
    * Telegram group: https://t.me/fairmarketES (Spanish)
    * General Mail: market@fair.coop
  * Project Manager and task: 
    * https://ocp.freedomcoop.eu/work/agent/512/
    * https://git.fairkom.net/faircoop/FairMarket

_ _ _

## OCP Development
A group for developers, administrators, facilitators, testers, users or interested people wanting to take on tasks in OCP development, support, project facilitation, architecture development,
  * You can donate for this project: fNwjJzntaQ8P95mappWQkEiSVrtDqDzj4X
  * Website on production: https://ocp.freedomcoop.eu/
  * Develop version: https://testocp.freedomcoop.eu/freedom-coop/
  * Sources:
    * https://github.com/FreedomCoop/valuenetwork
  * Programming languages: django framework and python
  * Contact:
    * Telegram group: https://board.net/p/OCPtesters
    * General Mail: coop@fair.coop
  * Project Manager and task: https://ocp.freedomcoop.eu/work/agent/547/


_ _ _


## Use Fair Coin Website
Project to keep track of the migration and issues
  * You can donate for this project: fHMex6pczhzXt6jSoVUNicoEgf5oefGGqH
  * Website on production: https://use.fair-coin.org
  * Develop version: http://www.usefaircoindev.fair.coop/
  * Sources: 
  * Programming languages: 
  * Contact:
    * Telegram group: https://t.me/usefaircoin
    * General Mail:
  * Project Manager and task: 
    * GITLAB: https://git.fairkom.net/faircoop/Tech/FairCoopSites/usefaircoinWebsite
    * OCP: https://ocp.freedomcoop.eu/work/agent/749/

_ _ _


## Fairlogin
Single-Sign-On with fairkom's fairlogin service. Here we track issues specific for the FairCoop community.
  * You can donate for this project: fPBxVEP9vgmxX6VcZ3MKrub8bgLgWELZp9
  * Website on production: https://id.fairkom.net/auth/realms/fairlogin/
  * Develop version: 
  * Sources: 
  * Programming languages: keycloak software
  * Contact:
    * Telegram group: 
    * General Mail:
  * Project Manager and task: https://git.fairkom.net/fairlogin/faircoop

_ _ _


## Fair.coop Main Website
To talk and work about technical issues/features about the fair.coop main website.
  * You can donate for this project: fGwvf72boDGy1yRx6qHboYNPevgjhdKVL4
  * Website on production: https://fair.coop
  * Develop version: 
  * Sources: https://github.com/faircoop/website
    * Content sources: https://board.net/p/faircoop_website_lite
  * Programming languages: Nodejs, react, markdown
  * Contact:
    * Telegram group: Faircoop website
    * General Mail: 
  * Project Manager and task: 
    * GITLAB: https://git.fairkom.net/faircoop/Tech/FairCoopSites/FairCoopWebsite
    * OCP: https://ocp.freedomcoop.eu/work/agent/748/
    * GITHUB: https://github.com/faircoop/website/projects
    * PAD with task: https://board.net/p/faircoop_website_bugs



_ _ _


## Forum Fair Coop 
To talk and work about technical issues/features about the forum.fair.coop and discourse for faircoop.
  * You can donate for this project: fHMs6PWfJb1k8UYk8WJqQPJKBjck84b3Ay
  * Website on production: https://forum.fair.coop
  * Develop version: 
  * Sources: 
  * Programming languages: 
  * Contact:
    * Telegram group: 
    * General Mail: forum@fair.coop
  * Project Manager and task: 
    * OCP : https://ocp.freedomcoop.eu/work/agent/751/


_ _ _


## Blog Fair Coop
To talk and work about technical issues/features about the blog.fair.coop and this drupal version for faircoop.
  * You can donate for this project: fFwVrdSDCUepBudUuMii3TwLFmqfbBb4BQ
  * Website on production: https://blog.fair.coop
  * Develop version: 
  * Sources: 
  * Programming languages: Drupal framework, php, html, css and javascript
  * Contact:
    * Telegram group: 
    * General Mail:
  * Project Manager and task: 
    * OCP: https://ocp.freedomcoop.eu/work/agent/752/

_ _ _


## Invoices for FreedomCoop
This is the project to follow track of the status of FreedoomCoop App to create invoices for members.
  * You can donate for this project: fHzUMMKWNk2NSHbpyYCnE2goU3STzKsg4e
  * Website on production: https://invoices.freedoomcoop.eu
  * Develop version: 
  * Sources: (some modules and odoo version 8)
    * https://git.fairkom.net/faircoop/Tech/FairCoopSites/invoices.freedoomcop.eu/tree/master
    * https://github.com/OCA/OCB/tree/8.0
  * Programming languages: python and javascript / bootstrap
  * Contact:
    * Telegram group: https://t.me/joinchat/AAAAAESKqs4Qq7QtuljSuA (Open coop Work)
    * General Mail: kispagi@fair.coop
  * Project Manager and task: 
    * GITLAB: https://git.fairkom.net/faircoop/Tech/FairCoopSites/invoices.freedoomcop.eu
    * OCP: https://ocp.freedomcoop.eu/work/agent/753/


_ _ _


## Kispagi
Kispagi (it means to pay with kisses in Esperanto) is the app to do the payment distribution for Faircoop.
It is free open source and here we can add the issues and improvements within the FairCoop ecosystem.

  * You can donate for this project: fUUTPe1QGWvbNYntv32cufUxKP5PtvUurk
  * Website on production: https://kispagi.fair.coop
  * Develop version: 
  * Sources: https://github.com/capiscuas/kispagi
  * Programming languages:
  * Contact:
    * Telegram group: 
    * General Mail:
  * Project Manager and task: 
    * GITLAB: https://git.fairkom.net/faircoop/Tech/Kispagi
    * OCP: https://ocp.freedomcoop.eu/work/agent/754/

_ _ _


## Calendar

  * You can donate for this project: fTDY9JRQnvg2HirsyA1CWUiNJmLmbYVxAF
  * Website on production: https://calendar.fair.coop
  * Develop version: 
  * Sources:
  * Programming languages: php 
  * Contact:
    * Telegram group: 
    * General Mail:
  * Project Manager and task: 
    * GITLAB: https://git.fairkom.net/faircoop/Tech/FairCoopSites/calendar

_ _ _


## Wiki
This project is for create and organize the wiki project trying to use our methodology

  * You can donate for this project: fXCAREqzqLLjuk7v186DjqePN1UNUiFLsx
  * Website on production: https://wiki.fair.coop
  * Develop version: 
  * Sources: https://git.fairkom.net/faircoop/Tech/wiki.git
  * Programming languages: html, markdown, css
  * Contact:
    * Telegram group: https://t.me/faircoopwiki
    * General Mail: 
  * Project Manager and task: 
    * GITLAB: https://git.fairkom.net/faircoop/Tech/wiki
    * OCP : https://ocp.freedomcoop.eu/work/agent/475/



_ _ _


## Wallet
Project of the FairCoop opensource wallet

  * You can donate for this project: fSDj6x1hdWxHwiyL7suVK2hS1LtWZ2DYXG
  * Website on production: https://wallet.fair.coop (not created yet)
  * Develop version: 
  * Sources: 
  * Programming languages:
  * Contact:
    * Telegram group: 
    * General Mail:
  * Project Manager and task: 
    * GITLAB: https://git.fairkom.net/faircoop/Tech/wallet.fair.coop
    * OCP: 
