---
title: 'Good Practices on WORDPRESS'
taxonomy:
    category:
        - manual
    tag:
        - Tech
---

WordPress is a Content Management System (CMS) and we can use it to develop good websites.

If we are using WordPress as one of our software tools we need to know how to do the right things together, in our style... FairCoop style. Always thinking about distributed, free content, free software, and use our system for the good of commons.

I'm proposing to follow these kind of rules starting here:

## Starting project:
  * We need to understand the requirements of the project:
    * Some questions:
      * Information to offer.
      * Functionalities.
      * Contents.
      * Way to use, way of communication, graphics...
      * Design.
  * Create a version control and issue tracker
    * Develop version
    * Testing version
    * Production version
  * Create a structure for file management inside using coherence, (e.g. if you upload a photo for the blog, create a folder in file management naming blog...)
  * In WordPress we have to take care about plugins because there are a lot with not much testing and maybe some that are not secure... please use ones with more stars and downloads.
  * Check which plugins are not used, and try to remove them.
  * Backup always
  * Think of the future

_ _ _

## Security recommendations:
  If you can edit .htaccess will be easier and cleaner because sometimes plugins are not necessary.

  * Backup, backup, backup and backup --> You can use a plugin but it's enough with All In One WP Security & Firewall, and we will use more things for security.
  * Keep WordPress and plugins up to date
  * Smart username and passwords (Don’t user “admin” as your username and choose a complex password)
  * Block bad bots (.htaccess / robots.txt / ipblock)
  * Always Use Secure Connections (sftp - SSL - ssh)
  * Redirecction and use SSL  --> Lets Encrypt and .htaccess or with plugin like "simple ssl".
  * Security plugins:
    * [WP Security & Firewall](https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/)
    * Follow the instructions to complete points in security
    * User Accounts (use a different username than your nickname, strong passwords...)
    * User login (block user too many attempts, force logout...)
    * Database security (have a good prefix and backup regularly)
    * Filesystem security (Check files and prevent use .php)
    * Firewall (Activate it)

  * [HTTP Headers](https://wordpress.org/plugins/http-headers) (Follow the instructions, read and think before if this configuration is good for you)
    * Security:
      * X-Frame-Options --> DENY on
      * X-XSS-Protection  --> 1;mode=block and on
      * X-Content-Type-Options --> nosniff
      * Strict-Transport-Security --> max-age=7776000(90 days)
      * Public-Key-Pins --> on and 90 days


_ _ _

## Performance Recommendations

- Cache performance on Wordpress:
	- Cache needs to change on server .htaccess - if you have access good if not ask for an administration.
	You can test for example this plugin [W3 Total Cache](https://wordpress.org/plugins/w3-total-cache/)

 - Minify JavaScript, CSS and HTML.
 - Enable gzip compression.
 - Specify image dimensions.
 - Optimize images.
 - Leverage breakpoints to download appropriate image sizes.
 - Keep Inline background images under ~4KB in size.
 - Remove unused components (css, javasript, etc).
 - Use efficient CSS selectors.
 - Download 3rd party scripts asynchronously.
 - Avoid empty src or href
 - Add an Expires or a Cache-Control Header
 - Put StyleSheets at the Top
 - Put Scripts at the Bottom
 - Reduce DNS Lookups
 - Avoid Redirects
 - Remove Duplicate Scripts
 - Make AJAX Cacheable
 - Use GET for AJAX Requests
 - Reduce the Number of DOM Elements
 - Avoid Cookies
 - Avoid Filters
 - Do Not Scale Images in HTML
 - Make favicon.ico Small and Cacheable

  * Check performance:
    * Google PageSpeeds -> https://developers.google.com/speed/pagespeed/insights/
    * Web Page test -> http://www.webpagetest.org/