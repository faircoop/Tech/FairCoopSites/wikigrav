---
title: 'Good practices to use DRUPAL'
taxonomy:
    category:
        - manual
    tag:
        - Tech
tittle: 'Good Practices on Drupal'
---

Drupal is a framework to develop good websites with many oportunities.
If we are using drupal in our software we need to know how to do right things together, in our style... Faircoop style. Always thinking in distribuited, free content and free software and change our system for the good of commons.

I'm proposing to check this kind of rules however we want but start here:

## Starting project:
  * We need to understand the requirements of the project:
    * Some questions:
      *Information to offer.
      *Functionalities.
      *Contents.
      *Way to use, communication way, graphics...
      *Design.
  * Create a version control and issue tracker
    *Develop version
    *Testing version
    *Production version
  * Create a structure for file management inside using coherence, (e.g. if you upload a photo for the blog, create a folder in file management naming blog...)
  * Try to use contributed modules and improve on community https://www.drupal.org/project/project_module
  * Check modules not using and try to reduce.
  * Backupn always
  * Think on the future


_ _ _


## Security recomendations:
   * Backup, backup, backup and backup --> You can use [Backup_Migrate](https://ftp.drupal.org/files/projects/backup_migrate-7.x-3.3.tar.gz)  is a good choice to program differents backups.
   * Keep Drupal and modules up to date
   * Smart username and passwords (Don’t user “admin” as your username and choose a complex password)
   * Block bad bots (.htaccess / robots.txt / ipblock)
   * Always Use Secure Connections (sftp   * SSL   * ssh)
   * File permisions (https://www.drupal.org/node/244924)
   * Redirecction and use SSL  --> https://www.drupal.org/https-information
   * HSTS --> https://www.drupal.org/project/hsts
   * Security modules: (https://www.drupal.org/project/security_review):
     * **Login Security:** Limit number of login attempts and deny access by IP address.
     * **ACL:** Access control lists for access to nodes.
     * **Password policy:** Define more security password policies for users.
     * **Captcha:** Block form submissions from spambots/scripts.
     * **Automated Logout:** Allows administrator ability to log out users after specified time period.
     * **Session Limit:** Limit the number of simultaneous sessions per user.
     * **Content Access:** Permissions for content types by role and author.
     * **Coder:** Checks your Drupal code against coding standard and best practices.
     * **SpamSpan filter:** Obfuscates email address to help prevent spambots from collecting them.
     * **Hacked!:** Check to see if there have been changes to Drupal core or themes.

   * Try to put HTTP Public Key Pinning (https://developer.mozilla.org/en-US/docs/Web/HTTP/Public_Key_Pinning)
   * Secure http headers --> https://www.chapterthree.com/blog/how-to-secure-drupal-http-headers
   * Drupal bugs register --> Put on none
   * HTST --> Better on .htaccess like on headers (Header always set Strict-Transport-Security "max-age=15568000")
   * File System --> Put on private path and private local server by Drupal
   * Put also a bit of Content Security Policy --> This is difficult because depend if we are using one script or another even a recomendation : Header always set Content-Security-Policy "default-src 'self' data: https: 'unsafe-inline' " (this is not completly secure, but is more restricted at least only script on https.)

  * Use check functions on output to prevent cross site scripting attacks
  * Use the database abstraction layer to avoid SQL injection attacks


_ _ _

## Performance Recommendations
  * Cache performance on drupal:
   * Caché active
   * Caduceus cache 1 day
   * compress cache page
   * join css files
   * join js files
   * Minify JavaScript, CSS and HTML.
   * Leverage browser caching.
   * Enable gzip compression.
   * Specify image dimensions.
   * Optimize images.
   * Leverage breakpoints to download appropriate image sizes.
   * Keep Inline background images under ~4KB in size.
   * Remove unused components (css, javasript, etc).
   * Use efficient CSS selectors.
   * Download 3rd party scripts asynchronously.
   * Avoid empty src or href
   * Add an Expires or a Cache-Control Header
   * Put StyleSheets at the Top
   * Put Scripts at the Bottom
   * Reduce DNS Lookups
   * Avoid Redirects
   * Remove Duplicate Scripts
   * Make AJAX Cacheable
   * Use GET for AJAX Requests
   * Reduce the Number of DOM Elements
   * Avoid Cookies
   * Avoid Filters
   * Do Not Scale Images in HTML
   * Make favicon.ico Small and Cacheable


Check performance:
   * Google PageSpeeds --> https://developers.google.com/speed/pagespeed/insights/
   * Web Page test --> http://www.webpagetest.org/


_ _ _


## Bibliography:

  * https://www.drupal.org/docs/7/site-building-best-practices/best-practices
  * https://www.drupal.org/security/secure-configuration
  * https://www.drupal.org/docs/7/security/writing-secure-code/overview
  * http://www.sparxsys.com/blog/10-drupal-best-practices-creating-solid-website
  * https://www.keycdn.com/blog/drupal-security/
  * http://openconcept.ca/sites/openconcept/files/DrupalSecurityBestPracticesforGovernment-0.92.pdf
  * https://www.drupal.org/docs/8/mobile/front-end-performance
  * https://developer.yahoo.com/performance/rules.html
