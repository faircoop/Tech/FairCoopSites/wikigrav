---
title: 'Manuals of software'
taxonomy:
    category:
        - manual
    tag:
        - Tech
---

We want to develop a list of our manual and recommendations for some of software we use:

<ul>
                                                            <li class="dd-item" data-nav-id="/technology area/software manuals/good_practices_to_use_drupal">
            <a href="/technology%20area/software%20manuals/good_practices_to_use_drupal">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>Good practices to use DRUPAL</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/software manuals/good_practices_to_use_wordpress">
            <a href="/technology%20area/software%20manuals/good_practices_to_use_wordpress">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>Good Practices on WORDPRESS</span>
            </a>
                    </li>
    
            </ul>