---
title: 'Technology Area'
media_order: ''
body_classes: ''
order_by: ''
order_manual: ''
---

====== Technology Area ======

We want to share most of our documents to understand how to work and the thing we are working...

### Know us better
  * [[en:tech:Our Policies]]
  * [[en:tech:Our Methodology]]
  * [[en:tech:Our Road Map]]
  * [[en:tech:Join us]]
  * We accept donations in faircoin: fGktNb5rdGKEnZWb8ngP9z15dNkZ1dnKUo

### Know our documents
  * [[en:tech:Assemblies:]]
  * [[en:tech:software-manuals:Software Manuals]]
  * [[en:tech:software-reviews:Software we use]]
  * [[en:tech:software-developing:Projects in course]]


