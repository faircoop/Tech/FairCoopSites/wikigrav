---
title: 2017/09/02
---

# Mumble meeting Sat 2 Sept 2017 2 p.m.
## Agenda proposal
### Short introduction of each patricipants and skills profile
  * Gui: nomad, sysadmin, social and friendly
  * Alfons (@berzas): Galicia, Spain. fair.coop sysadmin, ISPconfig LXD containers, volunteer sysadmin at Imaxinaria
  * Roland (@rasobar): Austria, communication, sysop, osAlliance, fairkom
  * Lynn (@fosterlynn): Wisconsin US, OCP (original) and api for Kamasi (django, graphql), analyst/modeler, vocabulary (ValueFlows)
  * Guy (@guyjames): From UK, living in Catalunya, P2P Foundation, CIC, web designer (mostly wordpress)
  * Arros(@arrosnegre): Catalonia. Backend and Frontend dev. Carsharing app, Faircoin2 data analytics. Go lang, node.js, angular, python.
  * Bernini: Italy, Frontend Dev & Designer, Javascript
  * Jorge (@cegroj): Spain. fair.coop sysadmin, ISPConfig LXD containers.
  * Yosu (@yosug): Basque Country, Spain. Computer hobbylist.


### Review Services List from Gui
  * https://ethercalc.org/FaircoopTechEcosystem 
  * please add responsible persons in each row
  * fair.coop estimate effort to repair / remove tweaks / hooks
  * prepare decision for Sept GA: repair or freeze and start from scratch (Gui facilitates meeting)
  * merge maps from market, use/getfaircoin  -> use openstreetmap / transformap (Onix(@Onix228) is the usefaircoin admin, Arros will write script to convert googlemaps data to openstreetmaps data)


### Hosting capabilities
  * fair.coop server: ISPconfig web + mail
  * fairkom: ISPconfig web + mail + gitlab + fairchat + sandstorm
  * imaxinaria: ...(is halted due personal reorganization) 
  * backup: CodigoSur.org possibility (Gui is part of CodigoSur, will ask)
  * We shoud have a common monitor: Nagios?


### Actions for the next week - what is most urgent?
  * list which sites are already migrated away from Site5, and which ones are remaining (Gui)
  * migrate remaining sites hosted at Site5 (Alfons and Cgroj)
  * http://fairtradenotforsale.net/
  * http://rizoma.fair.coop
  * information architecture - namespace for chats / projects etc (Roland can start, will ask bernini to join/review)


### Wiki system requirements
  * we don't need it for organizing our public information (Guy, Alfons think so)
  * can also mirror groups with single-sign-on
  * integrates etherpad or similar
  * care-taker in content and permission needed: guy would volunteer to start
  * a flexible option is to have an overview page of information resources, which points to wikis, owncloud folders, pads, etc


### Proposal for defining "Best Practices" for WordPress webadmins
Alfons proposes (and Guy, Roland support the idea) of defining some tips. Decide on caching, thumbnail size, etc. Put focus on loading speed. Alfons and Jorge would kickstart this, and then pass it on to the Tech group.

  * List of manually tweaked plugins would be very useful (meeting with bumbum)


### Issue management
  * Jorge and Alfons haven't been using a task management, coordinated physically between themselves (also started to use some sandstorm arround there from before but not so useful because we were 2) For other projects used Github, maybe agree to use Github from now need to talk with Jorge which options would be useful.

  * Lynn doesn't think issue management would come any time soon to OCP. If you're using Gitlab and doing a lot of issue management there... OCP is more economic operational tool (an ERP system for networks). Handles exchanges and supply chain, task management. I don't think we would write Gitlab inside OCP. We could figure how to get economic information from Gitlab

  * Lynn has been using Github for OCP development, but wants to migrate to some free software alternative. Would like to "eat its own dogfood" at some point by using OCP for handling work, but not yet, waiting for Kamasi interface.

  * Roland has been using different tools, including Redmine, finally are focused on Gitlab
 OCP is good for tasks and task and value distribution
 gitlab is good for issue management, can also record time spent which could be offerd to OCP via API


### fair.coop tech area manager candidates?
 Gui volunteers to practice in this position until October, then based on the experience continue (application as candidate to GA) or step down


### Remaining points (out of time)
  * Single-Sign-On perspective
  * how do we get the fairnetworks users in?
  * which services do we want to connect to fairlogin?
  * discuss this option in OCP
  * Establish scrum culture
  * think about who is the scrum master in each project and who is the project owner


### Gui What will I do:
Facilitate a meeting between @bumfresh and @berzas to gather information about why fair.coop WP plugins are tweaked, and in general anything that might help estimating the amount of work to "fix" the current WP install. Deadline: 1 or 2 weeks (depending on collective pace, which is currently uncertain to Gui)

Discuss inside CodigoSur (and with @berzas) the idea of backups exchange or agreement