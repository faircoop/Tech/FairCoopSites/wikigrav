---
title: '20 Dec 2017'
---
# FairCoop Tech Meeting

20 Dec 2017 19:00 GMT+1

Link to last minutes: https://wiki.fair.coop/en:tech:assemblies:/

Link to the life pad: https://board.net/p/tech-meeting-17-dec-20

## Attending
Al-Demon, Alfons, sfkls, Enric, Jorge, Guy

## Agenda
Reviewing the last assembly and what points we postponed I wanna propose this topic list:

1.- Reports Project in course [10min]

  * 1.1.- Problems with migration shit5 https://git.fairkom.net/faircoop/Tech/General/issues/61
  * 1.2.- Do static the old website 2017.fair.coop (not clear before)
  * 1.3.- Wiki (needs work to order documents we import and create a good structure) I want to create a telegram group
  * 1.4.- Some errors on main webpages at least for chrome
  
2.- Fixes Roles on tech [10min]
   * 2.1 Increase fix budget 500 to 600 for sysadmin
   * 2.2 Decrease facilitator hours from 30 to 20 a week

3.- Define our work methodology [Present first document: https://wiki.fair.coop/en:tech:our_methodology ] [10min]

4.- Define our Policies [Present first document: https://wiki.fair.coop/en:tech:our_policies ] [10min]

5.- Road Map [10min]
  * 5.1.- Finish the wiki pages with all our project in course
  * 5.2.- Communication campaign to ask for needs
  * 5.3.- Searching for new developers

6.- Questions time [10min]
 

* * *


## Meeting Minutes

### 1.- Reports Project in course

#### 1.1.- Problems with migration shit5
We need to finish but we have work to do with passwords... we hope we can finish on first days on January

#### 1.2.- Do static the old website 2017.fair.coop (not clear before)
We will archive the old website and convert to static HTML for resolve the content and the links but the entire wordpress go to another cheap VPS for administration things using another special subdomain.
We put this task on OCW.
@Guy said we can use wckr.github.io in a normal pc if we prefer for not use VPS- 

#### 1.3.- Wiki (needs work to order documents we import and create a good structure) I want to create a telegram group
@Al-Demon ask for help to order and create a good structure for wiki... @Al-Demon will create the link for a telegram group too

For tech, if you want to help... check this wiki page https://wiki.fair.coop/en:technology_area:start


#### 1.4.- Some errors on main webpages at least for chrome

We check some errors... maybe we can review the website in a dev environment | website sources:  https://github.com/faircoop/website
We need to update https://ethercalc.org/FaircoopTechEcosystem for have a map
https://wiki.fair.coop/en:tech:software-developing:projects_in_course

_ _ _


### 2.- Fixes Roles on tech [10min]
We propose for GA some changes on fixes roles 
 - sysadmin go to 96h x 2 people
 - facilitator go to 80h 

_ _ _

### 3.- Define our work methodology
We are agree for now https://wiki.fair.coop/en:tech:our_methodology

_ _ _

### 4.- Define our Policies
Good We can review deeply later, for now is only to know that it's there https://wiki.fair.coop/en:tech:our_policies

_ _ _

### 5.- Road Map

We can start with the proposal on agenda and see on https://wiki.fair.coop/en:tech:our_road_map

_ _ _


### 6.- Questions time

