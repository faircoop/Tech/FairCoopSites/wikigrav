---
title: 2017/10/18
---

# Tech meeting 2017/10/18
7 p.m. on Telegram FairCoop technical and mumble

## Attending:
Ale, bernini, Roland, Lynn (first part), Bob (first part),Niko, Michalis, Maro, Alfons, Jorge, Alex, Onix228, Ivan

## Issues
as #nn are referring to https://git.fairkom.net/faircoop/Tech/General/issues

## Agenda

### 1.- OCP subgroup status and report
1.1 OCP was agreed to be a FairCoop core tool for value exchange.
Links of the agreement should be provided and explain why the decision was taken and which were the timelines stablished. (Why should an assembly do this?: I request to change this point for an introduction and report from the subgroup -ale)

  -	1.1.1.- https://board.net/p/OpenCoopWork_Presentation#lineNumber=31
  -	1.1.2.- https://board.net/p/25thGlobalAssemblyFC#lineNumber=53


1.2 OCP timeline  (https://board.net/p/ocp-dev#lineNumber=14  has some actual roadmap...)

1.3 OCP API bug fixing (for kispagi) [not about bugs, more about old unclear commits from the testing branch -bob - p.s. I did not write the first part, just the clarification]

1.4 How to handle OCP server errors*

1.5 Focus more Developers effort on OCP Development *

1.6 Wallet inside may need security audit, in future use freecoin toolkit which is opensource, but is not ready and thus we use the chipchap service meanwhile (* move to OCP group?)

1.7 Bernini misunderstanding :D - OCP needs one more dev and testing by all

1.8 We plan to have an architecture - interconnection of tools discussion

### 2.- Migration or mirroring to fairchat.net #14, #10

2.1 Fairlogin now also proxies github id

2.2 mirroring

  * 2.2.1.- FairCoop general (already works since September) see https://fairchat.net/channel/faircoop-general
  * 2.2.2.- FairCoop Welcome Group (to do)

2.3 migration

  * 2.3.1.- CVN group (prepared)
  * 2.3.2.- chain admin (prepared)
  * 2.3.3.- FairCoin development (to do)
  * 2.3.4.- FairCoin Economic Strategies (to do)

2.4 General clarification how 3rd parties (such as fairkom, chip-chap, FreedomCoop, BotC) cooperate with FairCoop needed - task for General management

### 3 New website landing page proposal
Technical perspective (server, maintenance, access for editors, etc)

### 4 Calendar
We need a public calendar which can subscribe and show the ics calendar https://git.fairkom.net/faircoop/WelcomeSupportHumanCares/issues/6

### 5 Report on Madrid hackmeeting.
We had some Faircoop meetings there and created a telegram group to work on a nettiquete and best practices here the proposals refered to tech

5.1 Proposal of good practices for Faicoop ecosystem tools https://board.net/p/good-practices-architecture-faircoop

5.2 Actual needs of the area: were we are and were we want to go. taking in cosideration why we arrived here but for the moment is changing and there is the need to writ and develop   how and were we are going

  * 5.2.1.- metafore trip/project: - Define necesities - Identify why - Put the dates - Analize resources - Create plan  - Action - Review monitoring - Be always critic
  * 5.2.2.- Faircoop ecosystem websites: Outdated, not clear who is taking care of them, not documented (or not visible), not monitored, without version system, opacity. o work on improve securty and wp optimization we can work on https://git.fairkom.net/faircoop/Tech/General/issues/15 needs some corrections but is the general idea
  * 5.2.3.- Third part services: chip-chap, fairkom
  * 5.2.4.- Security and tools optimization: Proposal to create a group for pentesting and optimization of faircoop ecosystem tools. Proposal is create am autonomous open group that visibilize the problems that we could have and help to solve them. It could be funded with a donation wallet. group could be named "faircoop service pentesting & optimization". The aim of this group is to test and give possible solutions to help consolidate the tools we have.
  * 5.2.5.- Good practices and development: Audit, free software, APIs, documentation
  * 5.2.6.- in some moments we take decisions but later we need to evaluate, management and development has its phases and we are don't meet them
  * 5.2.7.- Ideas to establish how to function and facilitate function, how-to facilitate. Some tasks inside techs (maybe too much) are from third part Chip-Chap and Fairkom. analize each tool as project-manager, what fullfills and what can be changed, search for alternatives
  * 5.2.8.- newcomers: pay atention and cares
  * 5.2.9.- we also happily reviewed website proposal of Mario and Bernini giving some feedbacks
  * 5.2.10.- How we analyze: General values. Decentralization, Free Knowledge, Liberties , Software. [Python] Code. Documentation. Resources. Functions working. Solution plan. Alternatives already existing before creating something new.

### 6 Set up search.fair.coop with Yaci

### 7 usefaircoopdev
migrate the old users of use faircoop to the new http://www.usefaircoindev.fair.coop/ - could we use SAML or OpenID Connect in usefaircoopdev?

### 8 Faircoop server priorities and needs
There are 3 important things to-do that are block:

8.1 migrate fair.coop mailboxes https://git.fairkom.net/faircoop/Tech/General/issues/21

8.2 migrate remaining sites hosted at Site5  https://git.fairkom.net/faircoop/Tech/General/issues/3
  * 8.2.1.- pending to be migrated: https://board.net/p/faircoophosting#lineNumber=56
  * 8.2.2.- Enric will provide all DNS access to Alfons ASAP
  * 8.2.3.- Goal is to shut down site5 in 2017.

8.3 Implement a external backup solution 500GB
  * 8.3.1.- We look at  https://storj.io/
  * 8.3.2.- Hetzner 6€ per month, but we prefer off-site


### Points 5,6,7 are postponed to next tech area meeting.
(* = suggest we transfer these to the ocp subgroup assembly to save tech time here unless of general interest) I think is general interest the security of the wallet. 

### Assembly notes
1) The ocp group presented the setatus and links to access recent assemblies and roadmap. There was a discussion about wether ocp is a core tool etc, which ended up in the proposal of an architectrue group for discussing how an ecology of different economic alternaitve open source tools

#### skills required for OCP dev work:
Python/Django: From a few hours a week to full time. Ale / OCP group will create a small ad and pass to the dev group to send around where django devs might be found. https://board.net/p/OCP_dev_ad

2) Fairchat: Roland presented the plan to migrate or introduce fairchat as an alternative to telegram .We will now start informing people how to switch or mirror their groups.
