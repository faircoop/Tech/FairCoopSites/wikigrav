---
title: 2017/11/01
---

# Tech meeting 2017/11/01

7 p.m. CET on Telegram FairCoop technical and mumble

## Attending
Ivan, bernini, SFKLS, Guy, alfons, arros, Roland, Loose recordings (after 8 pm)

## Agenda
### 1. Look over notes from last assambly

  * There is some doubts from Bernini whether Fairchat is the right tool over Riot

It is priority for FairCoin to migrate (and a decision in the October GA) and we said we will mirror FairCoop telegram groups. Roland will extend a feature comparison table and if there are better alternatives, let's investigate them when they are user friendly and usable.

### 9 Al-Demon wants to present himself as a tech facilitator .

There was a debate about how facilitators that were proposed last month have been quite busy and couldnt do the role of project managers of the tech area.

His expertise is needed and his time of 30hrs per week to manage people working hands-on on various projects and giving support. So we suggest to propose him as a facilitator in the next GA. 750eur for the first two months as fixed income plus extra hours will counted from the OCW pool like anybody else. 2nd month maybe 20h. He will report weekly in gitlab or in the OCP


### 2 Review October works and define November's Goal

Debated about who will be responsible to update our WP versions for security issues. Guy will help with those updates of WP versions and plugins without affecting the modified ones. Will aso take a look and try to document the mess we have right now in our WP ecosystems. Guy will help (with the sync of sysadmins to give him access, sysadmins could eventually help if we clearify who is getting paid for wp mainteinance and we have a clear roadmap), but will not be held responsible if a manually-modified plugin has a security hole.

Orphaned web sites and current fair.coop will be made plain HTML.  We report in next GA.

### 3 Go public with the new faircoop websites and communication tools (wiki / blog / forum)
Proposal Link: https://board.net/p/faircooprenewal

  * 3.1 Link to chat? 
  * 3.2 How are local nodes represented?
  * 3.3 Calendar page see  https://git.fairkom.net/faircoop/WelcomeSupportHumanCares/issues/6 


## POSTPONED TOPICS FOR NEXT ASSEMBLY:
  * 4 Madrid hackmeeting
  * 5 Set up search.fair.coop with Yaci
  * 6 usefaircoopdev - user management

  * 7 Discuss Project Management methodology and decide what tasks are paid (go to OCW) and how to do it. Kispagi now works with OCP real tasks. kispagi.bitarkivo.org. Delayed, we?l wait till facilitator AlDemon do some research in our status.

  * 8 Wallets ( sergevi)

  * 8.1 wallet context : I was preparing on my side a technical work to propose a new wallet project for android and iOS ( especially iOS) in assembly ! Talk everywhere about it , even had talk with Thomas in dev group.. then I've learn only yesterday there was the same project developing already.  It's great but i think it shows a real hole in actual projects development management ? I wouldn't do all that if i knew.  I see there is 2 wallets already, nothing on IOS,  and no real project about this  subject (features, design, stories, future dev, etc). So, is faircoin wallet an important subject to people or not ? ( just want to know ). 

  * 9 presentation of discourse.fair.coop. installed by @arrosnegre. Invite all to register and play with it and give feedback.

  * questions: (do we want to import all the previous groups and topics from fairnetwork? or just users?). This should be sync for when we have the new website ready. Roland suggests to freeze fairnetwork in the archived 2017.fair.coop site - one reason is that a lot of content is not up-to-date anymore. It will be asked in GA about what to do.
  * 10 https://wordpress.org/news/2017/10/wordpress-4-8-3-security-release/ (what do u mean? or suggest? for what sites?) for every site for example looks like getfaircoin.net uses wp 4.7 released on december 2016 plus outdated plugins and themes, have information leak, not protected against X-Content-Type, X-Frame, XSS and probably more things. This is a less than 5 min report. I know almost all sites at faircoop ecosystem are on similar situation or even worse
