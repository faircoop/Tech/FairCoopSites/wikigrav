---
title: '2017/11/22'
---

# FairCoop Tech Meeting
22 Nov 2017

## Attending
Ivan Esperanto, Al-Demon, Roland, Mario, SFKLS, Jorge, Bernini, Alfons, Guy

## Agenda
Reviewing the last assembly and what points we postponed I wanna propose this topic for 15 Nov:

1.-Tools we need to organize: [35min]
  * 1.1  forum.fair.coop
  * 1.2  blog.fair.coop --> --> https://git.fairkom.net/faircoop/Tech/FairCoopSites/FairCoopWebsite/issues/2
  * 1.3  wiki.fair.coop --> Gitlab or Dokuwiki/wikimedia?¿  https://git.fairkom.net/faircoop/Tech/General/wikis/Wiki-Analisys

2.- Other needs: [35min]
  * 2.1 - Budget for 2nd backup approved. We need to start
  * 2.2 - Migrate mailbox from shit5 - Write a proposal to the owners with a deadline
  * 2.3 - cloud.fair.coop --> We need to decide if we want a own nextcloud like a software not like a service
  * 2.4 - usefaircoindev.fair.coop -->

3.- Summary of some conclusions of the analysis: [35min]
  * 3.1 - Methodology of analysis  --> I tryed to explain on the first part of the text
  * 3.2 - Create a team for security analysis and recomendations
  * 3.3 - Create methodologies for work together in tech
  * 3.4 - Create a new report special for projects manager on free software to get alternatives on OCW --> https://git.fairkom.net/faircoop/Tech/General/wikis/project-manager-analisys

4.- Questions time [10min]


- - -


## Meeting Minutes
### 1.1 - forum.fair.coop
@Arros installed to testing with @IvanEsperanto help... its working well and good on mobile phones... for now is on http://discourse.fair.coop/

We prepare the proposal for G.A. to use discourse like forum.fair.coop and freezing old forum on 2017.fair.coop

We are not consensus to migrate forum and chat we need to talk more... next wednesday 22 at19:00
- Roland can show us the live demo or usercases documentation for how we could replace fairnetwork and forum flow with Fairchat fixed threads...
- We could talk to the admin of the discourse of  https://confederac.io ,  he has done a good work there but has not much traffic, he is very motivated and maybe would like to join us with our discourse as part of the Welcome team.

Because this point needs to be solved I propose an order to talk and try to get focus on 3 min for each

  * 1 Showcase of fixed threads in fairchat: https://fairchat.net/channel/fairchat-general?msg=Sf2J7fwiGSakMxRvQ (up to 9 thread levels possible)  . I don't understand, I see the whole chat room too - it was the question if a URL can be pointed to a thread, like in a forum. 

  * 2 Discourse is nice technically, but opens a new big window for interaction. Can we handle this content-wise?

  * 3 Does discourse or a chat room meet the needs of local nodes?  It is not an OR question, but an AND question. a chat room excludes many people from our local node, they can't follow the rythm.

 Forum adds these features besides to our chat:
   - Asynchronous rythm, for people who doesn't have that much time to read that hundreds of msgs daily.
   - Email notifications/subscriptions and answering replying by email (has fairchat also)
   - Forum nettiquete different(you have more time to prepare your answer, you don't feel you are in the middle of some room of people looking at you)
   - Survey creations(can replace a lot of external doodles links)
   - Categories organization of the posts(so we don't have to create a new Chat group per area or project conversation)
   - Different type of community, different moderation.
   - Longevity. Forums tend to offer more longevity than chat rooms as the content of forums is archived and crawled by search engines
   -  We need to have some alternative for chat in Faircoop to give to people that doesn't like Chat and still want to participate in Faircoop.

 Chat pros:
   -  is something we already use daily and to which we put attention
   -  mobile clients with push notifications
   -  email notifications and answers (in next version with RC 0.60)


We agree to use discourse software for forum.fair.coop

### 1.2 - blog.fair.coop
Roland import to blog.fair.coop the last three blog posts in 3 languages and later we need to design theming with Bernini but not for relaunching time.


### 1.3  wiki.fair.coop
We go with Dokuwiki... with wysiwyg plugins -

We also talk about because @Al-demon use nextcloud and we need to continue talking about it into another meeting including evaluate the option to have a second level landing page with links to WIKI pages and PDfs.

I'll create the tasks on OCW



- - -


### 2.1 - Budget for 2nd backup approved.

@Alfons start to do the second backup in a distributed and encrypted way.

### 2.2 - Migrate mailbox from shit5
Write a proposal to the owners with a deadline - widely share the information that the mailboxes will be moved at the end of the month

30 Nov like deadline to save 80€/month


### 2.3 - cloud.fair.coop
We move this content to a wiki or another document and pass the own nextcloud to the next meeting


### 2.4 usefaircoindev.fair.coop

@Onix228 needs WP help or some script to import all content from https://use.fair-coin.org to , @Onix228 doesn't have much time to work.

Where is this server? Documentation of the website? Plugins, etc... @Onix should report on that. Otherwise the project is blocked.

Al-Demon will try to facilitate the migration to our servers and get more people to finish this work before 1:1 campaign

_ _ _


### 3.- Summary of some conclusions of the analysis
More or less we try to talk about the analysis but if anybody wants to talk in other assembly, please put in the agenda again.


### 3.2 - Create a team for security analysis and recomendations
Ok approved

### 3.3 - Create methodologies for work together in tech
Ok approved

### 3.4 - Create a new report special for projects manager on free software to get alternatives on OCW

We are creating testing versions of: Later we can put the links to join testing
  - Tuleap
  - Odoo
  - Taiga
  - Openproject
  - Kanboard


 - - -


### QUESTIONS TIME

@santiddt  is more eager to start discussing and working in reinforcing the faircoop wallet (with multisignature, several servers validating, own address wallet.fair.coop

he is the current maintainer of the ocp wallet which fails a lot.

we should create a project in gitlab for him to start commiting code in git and for discussing more technical. do we agree? anybody wants to join?

We will to create the project


--
Collaborate seamlessly on documents! This pad text is synchronized as you type, so that everyone viewing this page sees the same text. 
Create your own board and a (secret) name for it here: http://board.net This service is provided on fair-use with open source technology by fairkom. 
Consider a donation in Euro or FairCoin https://www.fairkom.eu/en/sponsoring#Donations for disk space and new features. Virtual hug guaranteed! 