---
title: '2017/12/06'
---

# FairCoop Tech Meeting
06 Dec 2017 19:00 GMT+1

POSTPONED TO 13 DEC 19:00 GMT+1

## Attending:

## Agenda
Reviewing the last assembly and what points we postponed I wanna propose this topic for 22 Nov:

1. cloud.fair.coop --> We need to decide if we want a own nextcloud like a software not like a service [35min]

2. Reports Project in course: [10min]
I wanna propose to review in a faster way the projects we have on course to know the status and if anybody need more help or facilitations things...
Maybe we can use a example to test this point.

3. Road Map for Faircoop Technical: Start to create this roadmap to know what we need to focus. [30min]

4. Improve Work Management & Communication with other areas
We agreed to release the new faircoop website and related tools in correspondence of the the 1:1 campaign. There was a consensus on the tech teams. But almost no tasks was done and today we are able to release only the website and a basic blog version.
 * When can we release the forum and the wiki?
 * When can we add other languages to blog?
 * When the blog will have style aligned to website?

5. How to find new OCP devs
We should talk about whether this is directly the responsibility of the Tech area or if we should create a sub-area. If it is the responsibility of the T.A., what are the pressing needs and how do we cover them? If we need to create a sub-area, who should be involved and how to go about doing it.Really???you need to create a subarea of the subarea of any task that is needed?? sorry but sound like I dont want to take care of this so lets add more bureocracy...so today your are goin to decide to create a subarea, next assembly you decide who are part otf the area, next month you can decide what this subarea of the subarea have to do and maybe in six months we can have someone starting to see how to find the dev?

 - IMO its a urgent need for whole faircoop. Solution could be quite easy: writing a call for devs with certain skills in the new blog (or forum) section of the website and share in social media and other networks. Article could be organized in the media communication/writers group...just need to ask for it

6.  Questions time [10min]
- - -


## Meeting Minutes

### 1  cloud.fair.coop --

Advantages:


Disadvantages:


_ _ _

### 2.

_ _ _


### 3. We can change the order ...
  * Define our policies and methodologies.
  * Put a date to finish the testing time for Project Manager.
  * Do a final report to recommended and install a PM for OCW thats cover all our needs.
  * When decided create continue develop project/team
  * Put a date to finish the testing time for ERP.
  * Do a final report to recommended and install a ERP for OCW, FreedomCoop... thats cover all our needs.
  * When decided create continue develop project/team
  * Create our Dokuwiki for wiki.fair.coop and create continue develop project/team
  * Finish our blog.fair.coop and create continue develop project/team
  * When decided cloud.fair.coop create continue develop project/team
  * Finish the migration from mails
  * Complete the backup system
  * Create a continue develop project/team for each wallet.
  * Create a continue develop project/team for each website
  * Create a continue monitor project/team for security
  * Create a continue develop project/team for discourse
  * Create a continue develop project/team for search page
  * Create a continue develop project/team for Mumble?
  * Create a continue develop project/team for Piwik?

### 4.

### 5.

### 6.

--

