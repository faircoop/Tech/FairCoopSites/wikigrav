---
title: 2017/09/20
---

# Tech meeting Sept 20 2017

## Participants: 
Guy, Ivan, bernini, Roland, Ale, Enric

## Preparing for the GA

### What is the Tech roadmap? See draft here:
https://board.net/p/25thGlobalAssemblyFC#lineNumber=172 - discussed, adapted and agreed

### Namespace proposal
discussed, adapted and no objections

### Candidates for facilitators

Fixed staff @santiddt @cegroj and @berzas  cost € 1400.-  per month, Sebastian Gampe @TonyFord would donate 1000 FC  p.m. to the tech area :-)

How much budget do we need then? 1400 + 3 facilitators + 100-200 hours tasks per month = 3800 p.m.

### Issue Management:

  * gitlab projects - missing permissions etc.?
  * add most urgent tasks in gitlab

### Tech facilitators:
Ale: facilitating ocp testers group, helping OCP tech tasks, documentation and usefaircoin admin. Also working on fairmarket and extension, outside of this group - so total I need is around 700, perhaps split as 500 for this and 200 for the other tasks.