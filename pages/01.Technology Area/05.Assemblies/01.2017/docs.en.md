---
title: '2017 Tech Assemblies'
child_type: docs
visible: true
---

2017 Tech Assemblies
<ul>
           <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-08-27">
            <a href="/technology%20area/assemblies/2017/2017-08-27">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/08/27</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-08-30">
            <a href="/technology%20area/assemblies/2017/2017-08-30">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/08/30</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-09-02">
            <a href="/technology%20area/assemblies/2017/2017-09-02">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/09/02</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-09-20">
            <a href="/technology%20area/assemblies/2017/2017-09-20">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/09/20</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-09-27">
            <a href="/technology%20area/assemblies/2017/2017-09-27">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/09/27</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-10-18">
            <a href="/technology%20area/assemblies/2017/2017-10-18">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/10/18</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-11-01">
            <a href="/technology%20area/assemblies/2017/2017-11-01">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/11/01</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-11-04">
            <a href="/technology%20area/assemblies/2017/2017-11-04">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/10/04</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-11-15">
            <a href="/technology%20area/assemblies/2017/2017-11-15">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/11/15</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-11-22">
            <a href="/technology%20area/assemblies/2017/2017-11-22">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/11/22</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-11-29">
            <a href="/technology%20area/assemblies/2017/2017-11-29">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/11/29</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-12-06">
            <a href="/technology%20area/assemblies/2017/2017-12-06">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/12/06</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-12-13">
            <a href="/technology%20area/assemblies/2017/2017-12-13">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>2017/12/13</span>
            </a>
                    </li>
                            <li class="dd-item" data-nav-id="/technology area/assemblies/2017/2017-12-20">
            <a href="/technology%20area/assemblies/2017/2017-12-20">
                <i class="fa fa-check read-icon"></i>
                <span><b></b>20 Dec 2017</span>
            </a>
                    </li>
    
            </ul>