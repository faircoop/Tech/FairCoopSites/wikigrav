---
title: 2017/08/30
---

# Tech Meeting Wednesday August 30th

## Attended: 
Arros, Ale, SFKLS, Enric, Gui, Guy J.

## Agenda:

Questions about OCP pad: https://board.net/p/questions-ocp+


### 1. Summary previous meetings

Pending items:

List of websites and owners: https://ethercalc.org/FaircoopTechEcosystem (done by Gui)
Replace Fairnetwork

### 2. FairNetwork (tools to communicate-coordinate)

Arros proposes forum of questions and answers like on StackOverflow - we agree we need something like this but the question is how we do it?
SFKLS proposes https://Sandstorm.io (the answer is it's better for small teams and internal work. We currently have sandcats.io from fairkom)
Enric says there is a plugin to link WordPress & Discourse - we agree to test Discourse
Tonight, Gui and Arros will install Discourse (https://www.discourse.org/) on a server, to allow the tool to be tested.
Install instructions: https://blog.discourse.org/2014/04/install-discourse-in-under-30-minutes/

### 3. automatic statistics
code: https://github.com/arnaucode/goBlockchainDataAnalysis
Would be good to be able to analyse statistics based on Faircoin blockchain. Arros asks which statistics would be interesting to have? Enric says those transactions coming from the FC ecosystem, rather than the other way round (those not coming from the FC community) would be the most interesting. We have some addresses which we can give to Arros to analyse. 


Some requirements based on arros's stats demo: 

    daily, hourly, monthly and "all time" (since fc2) stats on transactions

    trusted/non trusted addresses and transactions (based on exports of fc addresses from fairmarket, ocp and use faircoin)

    tracing the origin of a transaction: a history of where the money went.(you can only have one bittrex address for each user you register there)

Ideally this system could be on stats.fair.to or similar once done. It's currently on github.

### 4. carsharing app: 
code: https://github.com/arnaucode/carsincommonServer
Cars In Common

We talked about possible integration with local nodes for trust verification and possible contact with Som Mobilitat, an electric car coop in Catalonia. We will beta test it within the group. Also in future with cac.cooperativa.cat when it's opensourced. (question: can it be adapted to other social or local currencies?)

How to organise the tech group:
    Seek a coordinator (gui?)
    Meeting periodicity? What is the pace: we'd need to ask the IT people what makes sense. Once a month?
    Projects: ocp (monthly meetings), fairmarket, sysadmins (jorge and alfons coordinate), faircoin (thomas, santi, roland on non tech aspects), usefaircoin (onix,..). Then chip chap, who interact via botc groups. Dyne.org, fairkom 
    This needs coordination at the budget level,and perhaps to have a place to put together all tasks or needs

