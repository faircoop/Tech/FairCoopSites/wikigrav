---
title: '2017/11/15'
---

# FairCoop Tech Meeting
15 Nov 2017

## Attending
Al-Demon, Ivan,  Alv, Alfons, Roland, Enric, Indalo, Guy. Ale, Sebastian G, santi, alv

## Agenda
Reviewing the last assembly and what points we postponed I wanna propose this topic for 15 Nov:

 1.- Summary about Hackmeeting in Madrid

 2.- Tools we need to organizate:
  * 2.1 - search.fair.coop
  * 2.2 - UseFairCoopDev
  * 2.3 -- Wallets
  * 2.4 - forum.fair.coop
  * 2.5 - blog.fair.coop --> Drupal proposal Roland [import WP content?]  https://git.fairkom.net/faircoop/Tech/FairCoopSites/FairCoopWebsite/issues/2
  * 2.6 - wiki.fair.coop --> Gitlab or Dokuwiki/wikimedia?¿
  * 2.7 - Need a little budget for proposals and testing things for example... testing backups, testing p2p tools...
  * 2.8 - Migrate mailbox from shit5 - Write a proposal to the owners with a deadline
  * 2.9 - cloud.fair.coop --> We need to decide if we want a own nextcloud like a software not like a service
 3.- Summary of some conclusions of the analysis
  * 3.1 - Methodology of analysis
  * 3.2 - Create a team for security analysis and recomendations
  * 3.3 - Create methodologies for work together in tech
  * 3.4 - Create a new report special for projects manager on free software to get alternatives on OCW

## Meeting Minutes
### 1.- Summary about Hackmeeting in Madrid

At Hackmeeting between 12-15 October celebrated in Madrid, we met some Faircoop mates phisically and made some work group where we talked about many suff arround Faircoop ecosystem. We shared our impressions, proposals and concerns about actual sitiation mainly releted to techs. We can see this lasts weeks some proposals arised from our meetings there. We also created a telegram group "Faircoop best practices" where some of the HM Faircoop participants are working on make proposals about how to improve ecosytem tools and elaborated a netiquette approved on GA.  HM are great and inspirational places to meet between us and with new interesting people, so could be great to meet on next Hackmeetings or make our own ones.

Check the telegram group

Compilation of good practice https://board.net/p/good-practices-architecture-faircoop should be made more visiible.


- - -

### 2.1 - search.fair.coop

its not a priority for now but we can put on backlog

Yaci or Searx, low priority, we try to find a vounteer, issue created for the backlog https://git.fairkom.net/faircoop/Tech/General/issues/38


- - -


### 2.2 - UseFairCoopDev

@Onix is webadmin
Michalis: This new web site is pending to publish for some time now. There is a list of issues that needs to be arranged https://board.net/p/UseFaircoinDevReview

Since it is a priority to have it ready for the 1:1 campaign, please provide any Wordpress/PHP help or support to @Onix, in order to have it ready asap.

We need to review the @onix hours did before and will be paid from OCW

Deadline is 20NOV and we need help, please contact with @Onix228 or @ivanEsperanto or @arrosnegre for finish the migration before deadline.

  - - -

### 2.3 Wallets

  * Sergevi present their proposal. Alfons have some proposals about wallets.

sergevi : wallets subject.

Just in case i would be late for the assembly, here is what i wanted to ask to the group :

I believe wallet programs are more important than we could imagine.

More and more, people access the web with a mobile phone only and most "clients" using faircoins are people who are interested and convinced by the project, but not as active as the main group who comes everyday in telegram.

I think for those people, who are the most, the main interaction with the FairCoop world will be through the wallet. I believe they won't go so often to the websites, read the news, read forums or look for other projects in the FairCoop world.

  - Considering that, i'm surprised there is not a real "wallet" group as a real project. This group would work on the wallet as one of the main "app".

  - There is so much to do with wallets than just wallet functions :
    - Learning crypto currency , blockchain, what is specific to faircoin..etc. -> documentation
    - Integrated map of faircoin use -> where can i use faircoins
    - News  -> push important news . it's known to be far more efficient than blog

Besides, there is no real technical "app" project, with features, design, evolution, multi platforms, multi language, maintenance, support, etc...

We just started to work on an adaptation of breadwallet and Thomas start to have a look at it , but there is already two wallets ( android, electrum) and two other developers are working on a javascript one. But, nothing has been really thought in term of project management, design, etc.. 
Believe it should be .

  * What do you think about that ?
  * Thanks all :)

  * https://board.net/p/proposal-faircoin-wallets

Proposal to have the faircoop wallet pointing from wallet.fair.coop (to be more explicits) and add few features people are asking:
  - Securize it having it in another machine/container , sysadmins can tell what is best
  - Allow double factor login (Fairlogin supports it for example or just adding the support from some django extension).
  - Email notifications when somebody receives a faircoin transaction
  - Batch payments(with a text box list , having 1 address and amount per line for example, this would help when we have to do multiple payments at once (OCW, Local nodes, etc) and still have human verification)
  - When a transaction is done within Faircoop wallets, be able to keep the transaction comment or description to the other person's historial. (that way for example a local nodes wallet historial will know WHY receives a transaction from the sender's comments)
  - We can get more devs on hackaton https://fair-coin.org/en/hackathon2017


 - - -

### 2.4 - forum.fair.coop

  * Ivan/Arros summary and proposal [import or freeze like forum2017.fair.coop]

@Arros installed to testing with @Ivan help... its working well and good on mobile phones... for now is on http://discourse.fair.coop/

We prepare the proposal for G.A. to use discourse like forum.fair.coop and freezing old forum on 2017.fair.coop

We are not consensus to migrate forum and chat we need to talk more... next wednesday 22 at19:00


_ _ _


		WE CHANGE ORDER BECAUSE THE TIME

### 2.7 - Need a little budget for proposals and testing things for example... testing backups, testing p2p tools...

we thought to use https://storj.io/ or https://sia.tech/ as they are not censurable, open source and cheaper than other providers

The budget will be less to 10€/month for good p2p encrypted backup

Al-Demon ask @enric how to get this bugget

		WE HAVE TO BREAK BECAUSE THE TIME AND CONTINUE NEXT MEETING FROM 2.5


_ _ _


### QUESTIONS TIME
@ALV wants to talk about the 2FA and if possible to develop it soon as possible and how we want to do it.

We have developed on fairkom fairchat with 2FA login (which is based on keycloak) Anybody with a fairlogin account can activate it here https://id.fairkom.net/auth/realms/fairlogin/account/totp - any service which supports SAML or OpenID connect could use it immediately.

Ok to try to develop.

