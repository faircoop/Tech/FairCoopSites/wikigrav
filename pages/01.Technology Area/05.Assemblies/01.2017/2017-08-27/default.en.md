---
title: 2017/08/27
---

# Agenda Sunday 2017/08/27

  * Discuss what kind of project management methodology do we use. Waterfall method? In fact we work in an agile / scrum method. Not necessarily talking about how we implement it in OCP, but in general how we work in all projects.

    Waterfall method was the mainframe method in the 1980s (Specification, Implementation, Testing), was also applied in web, but did not work

    Scrum is an agile project methodology

    Scrum master: makes sure there are regular meetings

    Project owner is taking care of the relationship, a proxy of the customer (an assembly is finally deciding) - should be represented by a person! Concept should be changed to Caretaker, here.

    we may not need a caretaker if everything runs smooth, as we are part of the project ourselves. Model can be adapted to FairCoop ecosystem.

    double check issues: solved (scrum master) - closed (care taker). <-- these are the "two persons" that evaluate whether a task is done, in the "value distribution proposal" discussed in the 2017/08/26 general assembly

    work in sprints - rhythm and meeting tool / location is managed by scrum master

    Discuss how OCP can improve its features

    (user feedback has been collected yesterday, see https://board.net/p/OpenCoopWorkTech#lineNumber=100 )

    Workflow is Chain of tasks + recipes loosely coupling (this is not a feature, its how currently ocp works)

    Notifications inside the app, allow to reply by e-mail (like trello or gitlab allows that)

    emails from freedomcoop (not bankofthecommons)

    Projects easier configurable from work app.

    share a task.

    Comment on a task. also allow links to other project management systems tasks (github, trello, gitlab, chat)

    Make use of API calls e.g. to gitlab's spent time data per task, or notifying the process/task is done

    ways of invoicing and how to manage debts / charge (Chris writes use cases on github)

    Create Recipes for governance / income distributions

  * Responsibilities for services - updating the list https://ethercalc.org/FaircoopSitesMigration. https://board.net/p/faircoophosting

    Thomas wants to get rid of hosting use-faircoin.org

    We will clarify where all the websites are hosted, then move the WordPress websites which are still on Site5 to Alfons' server. Guy J and Gui to look at this this week in collaboration with Alfons and Jorge. At the moment we offer free website hosting to local nodes or other projects directly related to FairCoop. 

    Discuss how to technically replace FairNetwork (Discourse, Fairchat, etc...) or invade https://www.coeo.cc (please expain more) ... Loomio? 

    Things to migrate:  (2000 users, groups, collaborative documents, also discussions or archive them?

    Fairchat is Open Source and are working towards a federated structure from which the current centralised server setup. We agree that a distributed system is more in line with the FC principles.

    There is a plugin to link Discourse and WordPress: https://github.com/discourse/wp-discourse

    Rocket.Chat can be embedded in any CMS.

    Create a  Wiki with all the documents produced in fairnetwork

    Should be single-sign-on for forum, wiki and chat

  * discuss how can we make broader the faircoin development team (maybe include FairCoin Foundation in this discussion, which was formed in the recent months and agreed to have 200.000 FairCoins as a funding). Possible hackathon in Dornbirn


## Wiki Systems
  * Mediawiki
  * Dokuwiiki
  * gitlab

Effort is to design the namespace - have that similar to Chat groups. (this will be possible after we structured the information architecture of the whole faircoop ecosystem)
