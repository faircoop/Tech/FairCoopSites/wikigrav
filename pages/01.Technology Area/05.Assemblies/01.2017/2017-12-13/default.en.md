---
title: '2017/12/13'
---

# FairCoop Tech Meeting
13 Dec 2017 19:00 GMT+1

Link to last minutes: wiki.fair.coop/

## Attending
Al-Demon, Guy, Alfons, Onix, Bernini, sporos

## Agenda
Reviewing the last assembly and what points we postponed I wanna propose this topic list:

1.-cloud.fair.coop 
(We need to decide if we want a own nextcloud like a software not like a service)

2.- Reports Project in course: [10min]

3.- Road Map for Faircoop Technical: Start to create this roadmap to know what we need to focus. [30min]

4.- Improve Work Management & Communication with other areas

5.- Accounting program

6.- Questions time [10min]
 1) https://blog.fair.coop Do not have RSS?

- - -


## Meeting Minutes

### 1.-  cloud.fair.coop --
-- Really is a priority launch the same service when we have one running fine in the ecosystem? It's redundant and there are higher and more urgent priorities in the ecosystem to dedicate resources.

This is not a priority now we put on the list. We agree to add it to the Roadmap

**Advantages:**
    - We can order by files
    - We can share with date and password
    - We can use a DAV calendar to update and syncronize
    - We can sync with computers
    - We can federate with other nextcloud or owncloud

**Disavantages:** 
   - We need to maintain (but its easy maybe 
   - Already we have this service running fine with all the advantages detailed, why change it? Why should we dedicate resources for a redundant service? 

_ _ _


### 2.- Reports Project in course
I wanna propose to review in a faster way the projects we have on course to know the status and if anybody need more help or facilitations things...
Maybe we can use a example to test this point.
" That can help to have an global overview on which project is going on and where it is blocked and who needs help"
We agree to try in the next assembly if necessary. Better to use wiki?

_ _ _


### 3.- Road Map for Faircoop Technical: Start to create this roadmap to know what we need to focus.
Again, the Technical is a working group inside FairCoop, roadmap should be decided by General Assembly, or at least follow the guidelines from the General Assembly. The Tech area is not folllowing the General Assembly decisions, for example see the point 5 after 4 months approved. [Of course the roadmap goes to GA but we can propose what we can do it]
We agree to start the Roadmap in a wiki page on Gitlab. We will coordinate with the other areas to see what their tech needs are.

- Define our policies and methodologic.
We want to use the policies we talk in good practices[ I search later ]
And for methodologic: 

- Search new developers
We should talk about whether this is directly the responsibility of the Tech area or if we should create a sub-area. If it is the responsibility of the T.A., what are the pressing needs and how do we cover them? If we need to create a sub-area, who should be involved and how to go about doing it

IMO its a urgent need for whole faircoop. Solution could be quite easy: writing a call for devs with certain skills in the new blog (or forum) section of the website and share in social media and other networks. Article could be organized in the media communication/writers group...just need to ask for it

We agree to coordinate with the Communication Area to do a blog post asking for new devs. We will also ask in our own networks.

- Communicate with other areas to ask for tech needs.

- Task to add and order on PM: (agree with this)
    - Complete the backup system

    - Finish the migration from mails

    - Create our Dokuwiki for wiki.fair.coop and create continue develop project/team

    - Finish our blog.fair.coop and create continue develop project/team

    - Create a continue develop project/team for each wallet.

    - Create a continue develop project/team for each website

    - Create a continue monitor project/team for security

    - Create a continue develop project/team for forum.fair.coop

    - Create a continue develop project/team for search page

    - Create a continue develop project/team for Mumble?

    - Create a continue develop project/team for Piwik?

    - Create a continue develop project/team cloud.fair.coop  

_ _ _

### 4. Improve Work Management & Communication with other areas

  - We agreed to release the new faircoop website and related tools in correspondance of the the 1:1 campaign. There was a consensus on the tech teams. But almost no tasks was done and today we are able to release only the website and a basic blog version.
**When can we release the forum and the wiki?**

When we have the SSL certificate for the forum and the wiki should be in the next few days.

  - **When can we add other languages to blog?**
We have implemented 11 languages, translation of static pages is pending in the translation group https://git.fairkom.net/faircoop/MediaCommunication/issues/45

  - When the blog will have style aligned to website?
January, hopefully

** Extra issues:**
    - In forum try to add auth SAML

_ _ _


### 5. - Accounting program for Local Nodes?
ERPnext, Odoo, Dolibarr .. We have talked about the need of such a program, special need: multicurrency feature
We talk about guide to using software for control stock, supliers, order, different currencies, bank accounts, Fairspot is working as a trading shop in fairspot and Local Nodes

_ _ _


### 6. Questions time [10min]

1) https://blog.fair.coop Do not have RSS?
Simply append rss.xml
All blog posts in default language https://blog.fair.coop/rss.xml
Only ES blog pages https://blog.fair.coop/es/rss.xml

We can add like a task to review for the blog
