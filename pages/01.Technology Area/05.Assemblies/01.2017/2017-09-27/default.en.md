---
title: 2017/09/27
---

# Tech meeting Sept 27 2017

## Participants: (reconstructed from chat history):
Ale, Alfons, Roland, Ivan, Guy, Gui Onix, G, Juanse, santi

## Agenda
What is left from the GA25 Part 1 and the Tech meeting 20 Sept:

### Resources

### Candidates for facilitators

  * @altergui can only commit to dedicate ~4 hours per week during october and november, so would do that in a task-based dynamic (no fixed income)
  * @rasobar could offers "to fill the gap" if nobody else is jumping in.  He is commiting to work 80 hours per month, for 500 EUR
  * @ivanEsperantoKapis commits to work 20 hours per month for 125 EUR
  * @Onix228 is commiting to work 40 hours per month , for 250 EUR.
  * Fixed staff @santiddt (600) @cegroj (500)  and @berzas  (500) cost € 1600.-  per month, one more needed + 500.- to maintain various services (board, gitlab, fairlogin, fairchat, ...) = 2100.-
  * Sebastian Gampe @TonyFord would donate 1000 FC per month to the tech area :-)
    how much budget do we need then? 2100 EUR fixed staff + 1000 EUR divided between 4 facilitators   = 3100 EUR per month

### Support

  * Who can give 1st level support
        Ivan will help
  * Who can work on documentation
		Nobody awnser
  * Testing Discourse and FairChat as replacements for FairNetwork
  		Nobody awnser
  * Task management
  		Gui started a gitlab task list to dump everything that was lying around in pads

   https://git.fairkom.net/gui/SummerCampBacklog/issues these can be moved to their final destinations

   Maybe put here  https://git.fairkom.net/faircoop/Tech/Communication-Management - so docs should go in its WIKI also
   Ivan will merge, including Bumbums kanban board

### Notes from 25GA assembly on OCP:

Link to OCP roadmap: https://www.loomio.org/d/64A2kuro/ocp-dev-roadmap - looking for Python / Django developer, who is also coordinating

Bob Haugen, [28.09.17 21:02] I saw a lot about us supporting OCP, and havent read everything yet, but Lynn is now only working on the API for Kamasi (which might also be used for graphql integration) and I am only fixing bugs in process planning and logging, and helping lynn. That is all we are doing.