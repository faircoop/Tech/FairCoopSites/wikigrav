---
title: 'Open Coop Work Week Tech Meetings'
---
# Open Coop Work Week Tech Meetings
Summercamp Mont Soleil and follow-up meetings below

## Agenda:
Overview of tech work within FairCoop Ecosystem

### Priorities

### Introductions

  * **Gui (@altergui)** - sysadmin, wifi community networks, involved with Codigo Sur and AlterMundi, no previous tech experience with FC
  * **Christina** - Using FairMarket and OCP a lot, no tech experience
  * **Maro** - no tech experience but very involved in General Management and development of OCP on the user side.
  * **Enric**  - co-ordinator of tech in many FairCoop areas, doing a lot of admin/management. Tries to solve things himself, if can't, "calls a techie"
  * **Ivan (Bernini)** - working on OCP frontend
  * **Ivan Xeraco**- working on Python / Django development, P.Valencia local node
  * **Roland** (@rasobar) - IT open source consulting, sysadmin at fairkom, electronics, upcycling, wifi, amateur radio, telecom
  * **Maykel** - experience in DevOps, cloud, container, operating CVN node, in Madrid local node. Very busy so not able to commit to fully do one role.
  * **Chris** - sysadmin, software development, UI design, tech writing, very busy (lazy) so not able to commit to fully do one role. CMS and web app frameworks. Data modelling.
  * **Guy** (@guyjames) - basic linux sysadmin, interested in how the tech side is developing. Partly responsible for initially setting up fair.coop website. Also very busy but mostly with offline stuff.
  * **Michalis K.** - mostly content management on wordpress/drupal and some basic admin work. Interested in OCP management as part of setting and registering ecosystem tasks/work/tools.
  * **sporos**:  linux sysadmin (part-admin of cooperativas.gr WP), coordinator experience on Athens LN (4) OCP projects
  * **Ale**: web dev for far too long now doing tech/communication and fairmarket work.
  * **Arròs**: frontend and backend dev. Server APIs, Apps, Webapps, Botnets, DataAnalytics. Most used languages: Go, Nodejs, Angularjs. Active in Catalan Node.
  * **Al-Demon**:  linux sysadmin, webadmin, free Software developer , project manager, I can understand languages, php, html, css, c, nodejs, scripting, Active in Albacete Node.
  * **Onix228** : senior web dev,sysadmin and crazy tech activiste. Usefaircoopdev.fair.coop admin an dev, in FC ecosystem since 9 months.

### Overview:

#### FairCoin:
  * development team is too small (95% thokon00)
  * Santi looked at the Electrum server 
  * related to FairCoin:
	* FairCoin web site
	* Roland set up and maintains fair-coin.org web site
	* pending:
    	- paperwallet
    	- donate button creation script
    	- translations



  * FairChain
Thomas is working on the Fairchains project upon which CoopShares will ultimately be built. Victor, a dev from Berlin will also join this CoopShares project. It is an investment platform for cooperatives. Multicurrencies could be used on Fairchains.
Santi is working on FairMarket (Odoo) - he gets €600 per month to do this. He also supports Faircoin development.
African dev (from SeedBloom) is moving map on get-farcoin from Google Maps to Open Street Map, tried to do it automatically but there was no way, so is doing it manually.

#### OCP
XaviP -> Electrum wallet connection + bank of commons connection

Bob, Lynn, Pospi

bumbum + XaviP (chip-chap etc) since May 2017

bernini works on frontend, Lynn on API for that, Pospi and Bob consult, new version expected Nov 2017

Have separated backend and front end of OCP - goal is to have completely separate front end. Backend can share data with multiple applications. So we can build many client apps which can inter-operate together in a kind of ecosystem of apps. Kamasi is the tool for achieving this. In the new OCP you can create a project, and assign work, look at your balance. In the next version you will be able to make payments. You can see who did which task, and the state of the task, details etc. Plus the inventory which is all the resources owned by the project. You can concatenate tasks.

Chipchap are working on the Freecoin wallet integration with the FC2 blockchain to manage social currencies. They currently have funding but will need to apply for more funding or from BotC.

During the summer camp XaviP left the dev team and the following is a proposal for a new team: bob, lyn, bumbum, and bernini as developers who work on a series of weekly tasks on some set areas of development (major tasks). Other developers have offered support: santi has offered to maintain the blockchain aspects of ocp, pospi, dion and victor are able to develop in django sharing tasks and getting involved to various degrees. We would also seek 2 full time developers. This is 1) to take over XaviP's tasks, and 2) meet ongoing faircoop workgroup needs, fixing the simpler UI and navigation issues in using ocp within faircoop. Ale and Maro would help to prioritise tasks, translations/corrections and the creation of use cases, coordinate contact between OCP and the other faircoop work groups(issues and help/support), in the role of ocp testing coordinators. They would also take tasks together with the test team which functions as an area that prioritises tasks together with members of all these groups where relevant. Bob is happy to fix the simplest bugs in the work app that allow tasks to be usable. (ale 19/09)

https://www.loomio.org/d/64A2kuro

----------------

#### Semilla Social
Android app being developed by small group from Madrid (SemillaSocial.org), also multicurrencies, social currencies, CoopShares etc. Like a type of wallapop app. Looking into connecting it to Fairmarket.


_ _ _


#### Fairkom:
Roland is doing Fairkom projects: fairchat, fairlogin (single sign-on)

We have several maps but they are not connected, could be tied to the single sign-on idea and create one map which is a source for all.

_ _ _


#### FairNetwork
Discourse could replace FairNetwork. Roland says Fairchat could also function as a forum. Rocketchat can be configured so that a chat can be a public board as in a forum, but also as a private chat server. Threads can be linked also from outside (if read-only allowed for anonymous) like https://fairchat.net/channel/faircoop-general?msg=a3LJYfJaNM9CoevYn

List of websites - some do not need any maintenance, being simple one-page sites.

  * Enric - we need to avoid the situation where one tech makes unilateral decisions because they are working on it, setting their own priorities, which do not reflect the general assembly needs or decisions.
  * Roland - we should have fewer websites as there is a lot of maintenance, prefers to concentrate on fewer websites.
  * Enric - 'we' is a concept which is growing, we will have enough people to do the work.
  * Roland - we need to concentrate on the main websites and make sure they work correctly.
  * Chris - it's not a tech problem in many cases, it's a question of organization and personnel.
  * Enric - we have been running everything on a budget of €2,000 per month - so now we have more resources and can solve it with more people.
  * Gui - it may be just a question of coordination.
  * Enric - this is what we are doing here, co-ordinating, let's try to organize better now.
  * Roland - people get lost with too many websites.
  * Chris - it's a marketing question, not a tech question.
  * Maro - it's a lack of resources but also a coordination issue.
  * Bernini - we need a management flow protocol for working together in a decentralized way. We should a
  * Enric  - co-ordinator of tech in many FairCoop areas, doing a lot of admin/management. Tries to solve things himself, if can't, "calls a techie"gree on how to work then choose tools.
  * Ivan - some things are working, some not


#### Dedicated Server

Alfons, Jorge 400 € each

agreement was one sysadmin + dev, but both do sysadmin only

moving 25 web sites from site5, moving email and dns servers. implemented own tools and scripts (alternative to commercial ones


#### Priorities:

1. evaluate merging market.fair.coop and use.fair-coin.org: possible from a technical point of view - ask circular economy group if it makes sense [IvanXeraco will ask]
2. make one task management tool usable for production
3. identify what a task management tool should do
4. automatization of generating invoices for freedoomcoop members [IvanXeraco will take a look at it]
5. multicurrency in faircoop ecosystem:
6. get.fair-coin.org
7. fairpay app
8. single sign-on:
9. check all the platforms to see if they are single-sign-on capable
10. Improve monitoring: for example, an expired SSL cert or a website being quite slow is notified by end users in Telegram.
11. OCP: Get a structured feedback from OCP users
	OCP: when looking at faircoin add#lineNumber=344ress, you can see the balance and blockchain, but it's using the old blockchain
    hours definition and payment of the tasks
    two people check if done
    allow to comment how it was done from any project member
    OCP: simplify the ability to configure "use cases". Mapping the organization needs to a configuration of recipes and all different aspects of OCP that are configurable and approachable. Maybe make the UI self-explanatory, or improve the documentation.
12.    check for single-sign-on capabilities (SAML, OpenID Connect)
13.    unify mapping solutions present in fairmarket, use.fair-coin.org, OCP and fairnetwork
14.    FairCoin code, risk and security review lead: [Roland]
15.    Find candidates for a tech dev area management position
16.    Discuss what kind of project management methodology do we use. Waterfall method? In fact we work in an agile / scrum method. Not necessarily talking about how we implement it in OCP, but in general how we work in all projects.
17.    replace FairNetwork with something else
18.    Information architecture: how do we name folders, projects etc (Roland with communication group))
19.    make the "tech perspective" of the ecosystem overview, of all the websites and tools behind them (OCP, Wordpress, Odoo...) and protocols and shared data  [Gui will do this, with help of Roland] +3000
20.    This task list is missing the FairMarket tasks and Santi perspective, we should try to involve them somehow


#### Regarding task management tool

we need to solve:
  * tasks or requests from other areas to the tech team (i.e. "communication" needs help fixing something in wordpress backend)
  * tasks or requests coming internal from the team (i.e. "mapping is a chaos, let's make it better")

#### List of currently used/developed/mantained tools in the ecosystem

  * OCP (Django)
  * Wordpress
  * Odoo
  * ...

#### Resources:
Code Repositories and issue trackers
  * https://github.com/FreedomCoop/
  * https://github.com/FairCoin
  * https://git.fairkom.net/

#### Proposals from Telegram Tech Group
Alfons (@berzas)
Ok, about fair.coop website we agreed to improve it. Only on performance and security side, but some people involved on the ecosystem we think is better approach to start fair.coop from scratch and decide what we need and what we use there

Then our collaboration there should be less, i mean to "repair" what is there now and to have possibility to ugrade plugins is lot of work, there is no much info about what was done till now. If we decide to rebuild that webpage, we'll have our time for the server.

Talking about wp websites, i would like to propose to agree on common methodology and best practices: security, image optimization,...also that there should be defined roles like wp admins who take care of upgrades and ready to test or collaborate with f.e sysadmins

Another purpose is to document any work done, this simplifies stuff. We are doing that on sever side

If we decide we go ahead with what we have now on fair.coop we are not going to attend users, comments,...any non technical stuff. Only collaborate on performance and security

About wp admins, also if we know who they are, maybe there is an option to group websites by admin and lxd container. As most of you know we are working with lxd/lxc on the server

Michalis K.
Rebuilding fair.coop is a good idea imo but needs lot of discussion to decide and of course write whole new texts. In my opinion we could look into 2 parallel steps. 1) Rewriting the texts needed 2) Designing the new web page.

These two may be done asynchronously. For example, any new text could be implemented in the existing web page. The new web page then, whenever ready, may have incorporated all other faircoop tools as well.

