---
title: '2018/01/17'
---


# FairCoop Tech Meeting

17 JAN 2018 19:00 GMT+1

Link to last minutes: https://wiki.fair.coop/en:tech:assemblies:start/

Link to the life pad: https://board.net/p/tech-meeting-18-ene-17

## Attending
Al-Demon, Alfons,Cegroj, Guy, Bernini, Roland, Ale, Juan Sebastian

## Agenda

### 1.- Reports Project in course [10min]  

### 2.- OCP

#### 2.1 Follow the good practices of development we approve month ago we should be create at least three different enviroment for good dev.

#### 2.2 Split the databases to freedomcoop - Bank of the Commons - Faircoop OCW

#### 2.3 If other points are approved we need to calculate the resoures we need to use a good server for it.

### 3.- Question time [10min]

#### 3.1 Extension SA meeting yesterday placed this need (For a CRM)in the minutes:
_ _ _

## Meeting Minutes

### 1.- Reports Project in course [10min]  
move to the next meeting

### 2.- OCP

The OCP server has 4Gb RAM and 50GB HD space and is having problems of waiting and timedowns and falls... we need to find a solution...

#### 2.1 Follow the good practices of development we approve month ago we should be create at least three different enviroment for good dev.

 - Production
 - Testing
 - Develop

And if we can... use this structure for codding: 
 Master
 Release
 Develop
 Feature

To create a pull request you should create a new branch from master or develop, called feature/nameofthefeature 
Implement feature and

#### 2.2 Split the databases to freedomcoop - Bank of the Commons - Faircoop OCW
I think it's a different issue that we can discuss also in another meeting. Less urgent than the server upgrade and maintainance imho

#### 2.3 If other points are approved we need to calculate the resoures we need to use a good server for it.
All points together because 2.2 is not decided yet... We are agree to look for a new server more or less 16-24GB RAM and more than 100Gb for data space
E says the phone project has a server in Switzerland so we could look at using that, depending on resources.
Testocp is being turned off in february so that is a deadline for moving the test version to the new server
We agree to look for another provider than Hetzner for the new server after we had problems with them.


### 3.- Question time [10min]

#### 3.1 Extension SA meeting yesterday placed this need (For a CRM)in the minutes:
   
   
https://board.net/p/Extension_assembly_16th_January_2018#lineNumber=58
    
This app would serve any Area (CE requested so since some time as well)

Ok, we can start to develop this agent idea in a new group

requirements from EXT/CE: we need it kind of now, can it have configurable fields and be open source, and maybe link to APIs and things in future? so we will go for a simple solution to meet requirements in the pad: https://board.net/p/FC_collaborations_database_management_app we invite tech members to go into that pad and add tech requirements or comments and we will create an agents group or crm group to discuss platforms and invite people from other areas.

We agree to help for looking good solution for this development for Agents similar to a CRM 


#### 3.2 - Hetzner problem with server
@alfons
Ok, look we had a big issue with Hetzner. They provided a non standard kernel for the install and was quite pinfull to patch Meltdown. We ask for first time some help trought @rasobar because the contract is in his name. And it was so difficult to comunicate with Hetzner trough him. Hetzner didn't give us the right answer even they didn't pay attention to iur demand. Neither @rasobar was so collabortive. So we manged our own to solve the big problem now is everything ok. But I would like or to move the contract to Faircoop to have direct contact or even move from Hetaner away (also is NSA zone) we can migrate only when finish with shit5. What you think?


We talk with Roland too and we need to review contract, needs and maybe change of provider...

Last minutes Roland clear the contract:
FairCoop sysadmins have access to their own server. It is paid by the FairCoop member fairkom (the contract with Hetzner is with fairkom). We pay in Euro and get reimbursed in FairCoin.
 The problem is that FairCoop sysadmins can not send a support request to Hetzner directly by themselves, only the contractor can do that (just checked again their policies).
editado 
And Hetzner gives only ONE account for all admin issues (support requests, server resets, credit card info etc) to the contractor for all rented servers.

> Al-Demon: "is not decided in assembly for that, IMO We need to get a new consensus about this with new proposes, maybe in the next assembly"

* * *
