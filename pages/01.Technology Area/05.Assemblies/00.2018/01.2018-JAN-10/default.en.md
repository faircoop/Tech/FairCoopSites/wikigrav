---
title: '2018/01/10'
---

# FairCoop Tech Meeting
10 JAN 2018 19:00 GMT+1


Link to last minutes: https://wiki.fair.coop/en:tech:assemblies:start/

Link to the life pad: https://board.net/p/tech-meeting-18-ene-10

## Attending
Al-Demon, Alfons, Sfkls, Roland

## Agenda
Reviewing the last assembly and what points we postponed I wanna propose this topic list:

1.- Reports Project in course [10min]
- Blog status.
- Calendar status... 
- Forum status  
- Migration shit5 status
- Old website 2017 migration and convert HTML status



2.- Questions time [10min]
Stargate subdomain and hosting
Need to Install Piwik for control errors and improve our websites
Talk about security and maybe create a team about that
Hetzner support to mitigate Meltdown vulnerability

* * *


## Meeting Minutes

### 1.- Reports Project in course 

#### 1.1 Blog.fair.coop as gitlab project
 (I wanna ask to create a project for blog for share development, issues and track them. Now its only in OCP and is empty). https://wiki.fair.coop/en:tech:software-developing:projects_in_course#blog_fair_coop
Currently we have only one issue: https://git.fairkom.net/faircoop/Tech/FairCoopSites/FairCoopWebsite/issues/2
Is same Drupal instance like Fair-Coin.org  https://git.fairkom.net/drupal/faircoin/issues
Chris is attended for theming because is the more important issue for now and we try to put and report task on OCP/gitlab . 
For documentation / issue tracking we use this project https://git.fairkom.net/drupal/faircoin/labels and two labels: fair-coin.org or blog.fair.coop

As a comment since I put it in the assembly ... We need to do the task that we catch not delegate, because it is difficult to manage


#### 1.2 Calendar 
We have iCS for now, we try to improve with nextcloud but we don't agree if is a need for now... we wait and Roland help Al-Demon to try to use 


#### 1.3  Forum

(Fairlogin can be possible, Johannes which is in contact with Roland has access but got flu recently) - we continue tomorrow -R
We ask to communication group if they need something to participate more, like manual...


#### 1.4 Migration shit5 status

We wait for @Enr1c but can't now and we agree to make final deadline on 15 jan for changing the passwords if not provided


#### Old website 2017 migration and convert HTML status

The website is now in HTML and is upload but now we need to poweroff the WP .. we are agree to do and when we need the wordpress we put in a little VPS as we agree in the other assembly


### 2.- Questions time

#### 2.1 Stargate subdomain and hosting
@Biopablo wants a domain but not share the complete project...


#### 2.2 Need to Install Piwik for control errors and improve our websites
We agree to used and try to improve our websites monitor

#### 2.3 Talk about security and maybe create a team about that
We agree to create private group in fairchat for pentesting our apps

#### 2.4 Hetzner support to mitigate Meltdown vulnerability
Roland will help to talk with Hetzner

Last comment -- we need to get more people involved

{{tag>Assembly, Tech Area, piwik, migration, blog, website}}
