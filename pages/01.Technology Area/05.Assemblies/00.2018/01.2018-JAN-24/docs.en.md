---
title: 2018/01/24
taxonomy:
    category:
        - Assembly
    tag:
        - Tech
visible: true
---

# FairCoop Tech Meeting

17 JAN 2018 19:00 GMT+1

Link to last minutes: https://wiki.fair.coop/en:tech:assemblies:start/

Link to the life pad: https://board.net/p/tech-meeting-24-jan-17

## Attending
Al-Demon, Tib Kat, Michalis, Wonen, Eznomada,Alejandro, sporos, bernini,

## Agenda

### 1.- Reports Project in course [20min]  
- Blog
- Main Website
- Wiki
- Migration Shit5
- Migration Faircoop 2017 website
- Piwik (statistics)
- Communication for developers 
- CRM asked from wellcome
- We need developers / maintainers.- Getfaircoin need mantairners and Usefaircoin new website


.- We need developers / maintainers.-
Open call: We need Wordpress mantainers for Getfaircoin, join us here https://t.me/joinchat/C7QpPgvx-VVx0JR9jJubaQ
and Usefaircoin new website: Join us here: https://t.me/joinchat/C7QpPknN7vHbUWfsO7YfwQ


### 2  CommonRoutes server
(sorry, I won't be able to be during the assembly, I have another physical meeting)
introduction:
after being without time, now I have time to finish the CommonRoutes platform
These next two weeks, I'll have time to finish a first release of the CommonRoutes app (with server).
Currently, I have it almost finished, but I think that it would be good to have two weeks for testing with the people from the telegram group CommonRoutes.
Is possible use the server of FairCoop? or this is a 'side' project in the FairCoop scope, and we should use another server? (maybe payed with voluntary donations)
if not, we can get an outside vps server, for less than 4€/month, that can be collected through donations
technical caracteristics:
Nodejs: commonroutes main server
MongoDB: database
Go: images server
I don't know how many resources it will need, with the first month of use, seeing how many users we have, we will see how many ressources we need. I think that for the begining we will use small amount of ram and disk space.

### 3 New Subdomain Request
The proposal is to create a new subdomain: dashboard.fair.coop and redirects it to custom nameserver to use it as our new economic dashboard.
I'd like to use for this first period the server where I am working on, to speed up development, and in a second moment move the code to faircoops one.
Actual website: dashboard.economicecosystem.network
* * *
Alternative: workbench.fair.coop  (proposal from Roland, because we might use a dashbord for all the apps in future)


###  4.- Question time [10min]

_ _ _


## Minutes

### 1.- Reports Project in course  (Real time: 1h:30')

#### 1.1- Blog
We wait to Roland but we are trying to contact and improve to be more agile with fairkom collaboration... 
We need to improve style and finish the design and put easier to the communication group

#### 1.2- Main Website
We need help to managing 
The website is using gatsbyjs like framework...
We detect some errors we try to report better to analyze
Bernini wants to pass the work to another dev

#### 1.3 Wiki
We need more people to order and review the old documents ( For this we can create an article and talk with communication group e.g).
We need to finish the manual for editing
We need a design
[enildo wants to help]

#### 1.4 Migration shit5

Continues process

#### 1.5  Piwik
This is the free software for statistics... now chage the name to matomo
Al-Demon installed the last week and we are trying to use but still in testing
We need to put like a script in main website, blog and where we want to know better statistics...
We can coordinate too with @TonyFord
We are using piwik.fair.coop but is good to statistics.fair.coop or ecosystem.fair.coop
We can discuss possibilities to open the data and or put fairlogin there Maybe join to faircoop data analytics group 

(Data Analytics https://t.me/joinchat/AKm2tQ1K7hHIXl0hVFgevg It's on our chat group's list @juanselink was trying to coordinate the creation of a statistics web page, since Summercamp there.)


#### 1.6 Communication for developers 
We need devs in several projects, but I think is difficult to explain and we need ideas to attract them
We want to create a pad for put some long goals focus in a good ROADMAP


#### 1.7 CRM asked from wellcome
Is not for welcome, looks like is for extension... maybe santi is helping... in odoo

#### 1.8 .- We need developers / maintainers getfaircoin and usefaircoin 
Open call: We need Wordpress mantainers for Getfaircoin, join us here https://t.me/joinchat/C7QpPgvx-VVx0JR9jJubaQ
and Usefaircoin new website: Join us here: https://t.me/joinchat/C7QpPknN7vHbUWfsO7YfwQ


### 2  CommonRoutes server
We agree to tell sysadmin to create a container for this server.


### 3 New Subdomain Request

We agree for using agent.fair.coop to point @bernini's server to continue develop a dashboard to conect OCE and OCP and other apps from ecosystem dashboard.economicecosystem.network


###  4.- Question time [10min]

No question this night
