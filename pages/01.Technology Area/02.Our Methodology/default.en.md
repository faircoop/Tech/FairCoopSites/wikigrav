# Faircoop Free Software Develop Method

## Coding
We use a git system to synchronize the work when somebody are working together.

Now we are using [[|github]] and [[https://git.fairkom.net/faircoop/|gitlab on fairkom]].

### Structure of Codding in git
  * Master
  * Release
  * Develop
  * Feature

To create a pull request you should create a new branch from master or develop, called feature/nameofthefeature

Implement feature and unit test and make a pull request after the pull request is accepted we merge the branch on develop and if unit/integration test passes it goes to release

## Environment
We want to use three different environments with the same hardware to use, test and develop our software. 
  * Developing (using a develop version of the software, testing and develop the new features).
  * Testing version (using the last release with a new feature)
  * Production (stable last release)