---
title: 'Our policies'
taxonomy:
    category:
        - Policies
    tag:
        - Tech
---

We work on Faircoop with good practices like this:

Based on our principles we see it necessary to analyze the dimensions and structures, which tools we use and how we are developing our ecosystem, these basic principles agreed upon are: (Integral revolution, peer2peer, cryptomonniques, free knowledge, hacker ethics, degrowth, good living...)

We must use a Project Management system to address each area with sufficient criteria to avoid deviating from the critical spirit, free software practices, the integral revolution and hacker ethics so that. 

Our tools must always meet the following criteria:

   - Free software to promote cooperation, self-management and freedom.
   - ICT tools are not an end in themselves but a means.
   - We must be able to adapt to changes so we need version control.
   - The operation, processes and tools must be self-learning and therefore documentation and courses available.
   - We must allow the territorial structure of Glocal -- languages, different cultures that connect to the global.


We propose this methodology for each area... starting with Tech and as it is cross-cutting the needs should be asked to all areas clear.

  * 0.- Define minimum needs.
  * 1.- Dates to cover the need.
  * 2.- Analysis of tools and methodologies used to date.
  * 3.- Available resources.
  * 4.- Improvement plan.
  * 5.- Improvement action.
  * 6.- Monitoring.
  * 7.- Criticism and review.

Starting with Tech area and all the tools that depend on it. Using for example this index of values in order to try to be objective:

   - If he fulfills the four freedoms, one point.
   - If decentralized - One point.
   - If distributed - One point.
   - If it is low power consumption (relative to others) - One point
   - If it is KISS and modular (one use a task) - One point.
   - If you have version control. One point.
   - If we already have resources - One point.
   - If there is complete documentation - One point.
   - If it already meets the need to cover - One point.

In addition to the technical analysis, a code of conduct for communication and development based on the Netiquette and a reputation system are proposed in order to give the necessary confidence to critical tools.
Netiquette - https://git.fairkom.net/faircoop/MediaCommunication/wikis/FairCoop_Chat_Netiquette_EN
Reputation system -[https://board.net/p/reputation_protocol_proposal] 


- - -

NOTES AND CLARIFICATIONS USED TO MAKE THE PROPOSAL:

# Dimensions
Based on our principles, I think it is necessary to analyze the dimensions of how we communicate, what tools we use and how we are developing our ecosystem.

## Principles / values:
  * Integral revolution, peer2peer, cryptomoney, free knowledge, hacker ethics, degrowth, good living

  * Hacker ethics:
    * Unlimited access.
    * Free knowledge.
    * Antiauthoritarian.
    * Decentralization.
    * Meritocracy? How far? let's set maximums for what does not become privileges. https://board.net/p/reputation_protocol_proposal >> proposal on going @tereseta
    * Digitalization.

  * Integral Revolution:
    * Social Transformation.
    * Interest in the common good and in being right with oneself.
    * Get rid of materialism.
    * Cooperation and solidarity in social transformation.
    * Transition based on the day to day and for being ever closer to turning utopia into reality.
    * The direct relationship between practical action and its theorization.
    * Inclusive and inclusive co-operative for all of society.

  * Society:
    * Equity and social justice.
    * Equality in diversity.
    * Self-realization and mutual support.
    * Commitment and self-evaluation.
    * Share our practices with society as a whole.

  * Economy:
    * To attend to the needs of the people above all other interests and each one contributing according to their possibilities.
    * Currency is a system of measuring exchange between people in the community, excluding accumulation as an objective.
    * Other non-monetary forms of exchange are promoted: free economy, direct exchange, community economy.
    * Establish economic relations between producers and consumers: the cooperative will provide guidance for calculating fair prices on the basis of their costs, their own needs and those of consumers.
    * The cooperative shall inform producers of the needs of consumers to guide their production.
    * ECOcoops will never be convertible into euros and no interest is accepted on your loan.

  * Ecology:
    * Ecology and Permaculture.
    * Decrease and sustainability.
    * Political Organization.
    * Democracy: direct, deliberative, participative.
    * Self-management and decentralization.
    * Transparency.
    * Subsidiarity: from local to global.

  * Assembly.

  * Decrease: Revaluing: It is a question of replacing global, individualistic and consumerist values with local, cooperative, and sustainable values.