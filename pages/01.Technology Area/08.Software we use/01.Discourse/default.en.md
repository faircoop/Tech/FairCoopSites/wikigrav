# Discourse
It is a software very used to forums, a new generation after the own decline of the forums php that had their peak in the early years of the millennium is created in 2013 and it has done a good gap between the tools of communication, including bi-directional and with subsequent collection, as was the e-mail.

It is developed in ruby and javascript, with a participation of at least 600 active contributors. In the final analysis is still maturing, but with way of firm.


## Requirements
  * Ruby on Rails
  * Ember.js
  * PostgreSQL
  * Redis

## Comparisons with similar
  * Pros:
      * Focused on the conversations.
      * Groups are very broad.
      * Public access.
      * Markdown and HTML typing.
      * Share files.
      * Mailing list.
      * Surveys.
      * Multilanguaje.
      * Chat.

  * Cont:
      * Meritocracy.
      * Gaming style.


## Rating[6/9]
  * LAST UPDATE: -->30 - Oct - 17
  * PLUGINS/MODULES: YES
  * DESCENTRALIZED: NO (only on cloud server)
  * DISTRIBUITED: NO
  * CONSUMPTION:
  * KISS-MODULAR: YES only a forum
  * DOCUMENTATION: YES https://meta.discourse.org/
  * 4 FREEDOMS FS: YES GPLv2.0
  * CONTROL VERSION: YES Github --> https://github.com/discourse/discourse

## Used in FairCOOP in
  * Our Forum [[https://forum.fair.coop|fair.coop]]

## How we use it
  * Currently, there is No methodology maintenance and development yet.
  * Not being a system of communication and p2p don't be a mature software I'm not sure if it worth the effort to convert people to use this software as a communication system of faircoop but it could work at least in the version messaging not immediate...