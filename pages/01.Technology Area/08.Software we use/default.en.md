# Software tools in FairCOOP

This report aims to know a little better our tools with their characteristics, advantages and disadvantages.

Also we will see how we are using and how we should use them... I hope this will serve us to make better decisions.

If any of these tools has become essential to my opinion we should enable a version of their own and controlled by the assembly tech and does not use external services.

That is, if each one of them with their control system versions and system development based on free software.

(Dev - Test - Production)

## Try to explain analysis report

I put some value numbers from quality and good practices on software next to the domain... this analysis is for us and in general we need to improve our methods and documentation.

One of the categories to evaluate should be take care about electric consumption but for now we don't have enough information.

Try to explain points:
- SOFTWARE: [ Name of installed software ]
  - UPDATE: [ If is update the software and when ]
  - PLUGINS/MODULES: [ Can we use modules separated about core source code ]
  - DECENTRALIZED: [ If we can federate servers or sync backups ]
  - DISTRIBUTED: [ If is a p2p tool ]
  - CONSUMPTION: [ How energy consumption related normal tool ]
  - KISS: [ Keep It Simple Stupid  - One tool for one purpose ]
  - DOCUMENTATION: [ If there good documentation with public access ]
  - 4 FREEDOMS FS: [ If we liberate the code we change on modules ]
  - COVER NEEDS: [ If now is covering our needs ]
  - CONTROL VERSION: [ If we have a control system with almost 3 versions develop-testing-production ]


- - -

## We analyze this software:
  - [[Drupal]]
  - [[Wordpress]]
  - [[Odoo]]
  - [[HTML5]]
  - [[Telegram]]
  - [[RocketChat]]
  - [[Discourse]]
  - [[Nextcloud]]
  - [[Etherpad]]
  - [[Ethercalc]]
  - [[Wekan]]
  - [[Webcalendar]]
  - [[ValueNetwork]]
  - [[Mumble]]
  - [[KeyCloak]]
  - [[Yacy]]
  - [[Storj]]
  - [[LXD Server]]