# Etherpad
Software for collaborative writing of documents in real-time, essential in a horizontal organization like ours. Use the browser as a sheet of paper and we have to provide you with the tools for easy formatting of the text. Allows you to use names, notes, comments and chat to organize the structure of the text.

The project was born in 2008, and the project can be considered mature and even having a few contributors the documentation is clear and orderly.

## Requirements
  * Node.js
  * curl
  * python
  * mysql does?
  * Ngix?do


## Comparisons with similar
  * Pros:
      * Synchronizes texts.
      * Allows collaborative editing
      * Markdown and HTML typing.
      * Exporting to other formats
      * Chat included.
      * Embeddable in other applications.

  * Cont:
      * Unsafe by default.
      * Difficult to moderate.
      * It is not p2p


## Rating[6/9]
  * LAST UPDATE: --> 4 - Nov - 17
  * PLUGINS/MODULES: YES
  * DECENTRALIZED: NO
  * DISTRIBUTED: NO
  * CONSUMPTION:
  * KISS-MODULAR: Yes one task - collaborate writing.
  * DOCUMENTATION: YES -->https://github.com/ether/etherpad-lite/wiki
  * 4 FREEDOMS FS: YES Apache 2.0
  * CONTROL VERSION: YES

## Used in FairCOOP in
  * For the 90% of our texts and assemblies we use several third-party servers as riseup.net - board.net - disroot.org and other....

## How we use it
  * Currently, there is No methodology maintenance and development.
  * I see a need to integrate this software in the community Faircoop to meet the need of shared writing and I propose to join the server of etherpad at nextcloud to be able to sort by folders such writings to locate them later if this is possible.