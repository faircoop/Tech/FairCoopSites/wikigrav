# Drupal
Free Software written in php that is used as a framework for web applications, mainly blogs and pages in multisite. Has been in operation since the year 2000, and is currently developing version 8 and only for security bugs version 7.

Currently, there are about 110,000 developers involved.

So we can consider it a mature software and well structured.

## Requirements:
    - Mysql server
    - Apache or Ngix Server
    - Linux
    - php

## Comparisons with similar:
  - Pros:
    - Very modular.
    - Great documentation.
    - Code very clean.
    - Large-capacity user management
    - Community is very active and responsible.
    - Very integrated with the idea of free software and its business model and common benefit.
    - More secure than wordpress for the cleanup and the community.
    - Each content type, a different form.
    - We have separated the content of the continent.
    - Great backend.
    - Management by SSH.
    - Text editor for markdown / wyswyg

  - Cont:
    - The editors are not convinced, it is something more complex than WP.
    - It is not WP.
    - It is not fashionable.
    - php is out of fashion.


## Rating:[6/9]
    - LAST UPDATE: V7 --> 21 June 17 V8 --> 30 Oct 17
    - PLUGINS/MODULES: YES
    - DESCENTRALIZED: NO (only on server) We can copy and balanced another site
    - DISTRIBUITED: NO
    - CONSUMPTION:
    - KISS: YES
    - DOCUMENTATION: YES https://www.drupal.org/documentation
    - 4 FREEDOMS FS: YES GPL2.0
    - CONTROL VERSION: YES

## Used in FairCOOP in:
    - [[https://fair-coin.org| fair-coin.org]]
    - [[https://blog.fair.coop|blog.fair.coop]]

## How we use it:
    - Currently, there is No methodology maintenance and development.
    - We should review both the system development, attempt to separate the content of the code as much as possible and review all the modules used to see if we can move to the branch 8, because the 7 as development continues, only the failures.