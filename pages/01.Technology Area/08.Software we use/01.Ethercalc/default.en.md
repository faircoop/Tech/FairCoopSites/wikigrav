# Ethercalc
Software for collaborative writing of spreadsheets in real-time, essential in a horizontal organization like ours. Use the browser as a sheet of paper and we have to provide you with the tools for easy formatting of the text and calculation. Allows you to use names, notes, comments and chat to organize the structure of the text.

The project was born in 2012 and not the project can be considered mature since it has a low rate of contributors, the system is not stable at all but in a good way.

## Requirements
  * Node.js
  * curl
  * python
  * mysql does?
  * Ngix?do

## Comparisons with similar
  * Pros:
      * Synchronizes spreadsheets.
      * Allows collaborative editing
      * Exporting to other formats
      * Embeddable in other applications.

  * Cont:
      * Unsafe by default.
      * Difficult to moderate.
      * It is not p2p
      * Forms do not work well.

## Rating[6/9]

  * LAST UPDATE: --> 4 - Jun - 17
  * PLUGINS/MODULES: YES
  * DECENTRALIZED: NO
  * DISTRIBUTED: NO
  * CONSUMPTION:
  * KISS-MODULAR: Yes one task - collaborate calc.
  * DOCUMENTATION: YES -->https://ethercalc.net/#book
  * 4 FREEDOMS FS: YES CPAL
  * CONTROL VERSION: YES

## Used in FairCOOP in
  * For some worksheets necessary in our texts and assemblies we use several third-party servers as ethercalc.org

## How we use it
  * Currently, there is No methodology maintenance and development.
  * I see a need to integrate this software in the community Faircoop to meet the need of shared writing and I propose to join the server of etherpad at nextcloud to be able to sort by folders such writings to locate them later if this is possible.